# gTOFfee: applying the energy span model to reaction networks
Diego Garay-Ruiz, 2020

Institute of Chemical Research of Catalonia

Prof. Carles Bo Research Group

## Introduction

gTOFfee is a Python implementation of the Energy Span Model developed by Sebastian Kozuch (Kozuch WIRES Comp. Mol. Sci. 2012), recently reformulated in terms of graphs (Kozuch ACS Cat. 2015, Solel, Tarannam & Kozuch ChemComm 2019) as an extension from linear profiles to arbitrarily complex reaction networks.

The core of gTOFfee is separated into modules:

-   ReaxGraf (**rg**). Handles reaction network I/O and processing, transforming user input into NetworkX.Graph objects. These graphs are treated to generate the required subgraphs (mechanisms and spanning trees) following the Energy Span Model equations, producing a MechanismContainer object.
-   gTOFfee (**gtof**). From a pre-treated reaction network, expressed as a MechanismContainer, applies the ESM to compute the TOF for every single mechanism arising from the network, with or without concentration effects.

A paper detailing foundations and use cases of gTOFfee is available: D. Garay-Ruiz and C. Bo, *ACS Catalysis*, **2020**, 10, 21, 12627–12635, DOI [10.1021/acscatal.0c02332](https://doi.org/10.1021/acscatal.0c02332). 

A basic use manual is available under `manual/gTOFfee_guide.md` inside this repository.

## About the networks
In principle, any kind of reaction network can be passed to gTOFfee, with the only condition of having *exergonic* reactions.

The input is given as plain text (see `manual/gTOFfee_guide.md` and `examples/` folder for reference). 

**Important note**. For every cycle in the network, the final node must have $`G_{reaction}`$ as its energy. Thus, it will correspond to the fully recovered catalyst, only that shifted in energy. 
Also, the *closing edge* which connects the final node with the start point of the cycle must ALSO have $`G_{reaction}`$ as its energy. Is in this way that $`G_{reaction}`$ can be determined for every mechanism, adjusted to different reactant/product concentrations, and allow for the treatment of different products in a single network.
Also, this last node (*recovered catalyst*) must only have 2 connections: to the initial catalyst (start node) and to the last on-cycle species, from which it departs. 

## Additional features
A third module, InteractProfiles (**intp**), based on the Panel library for interactive visualization of data, has been included. Through InteractProfiles, it is possible to easily generate interactive plots for the free energy profiles corresponding to each mechanism: the TOF and effective activation energy values of the system update automatically reacting to user-specified changes in transition state energies and concentrations. 
This feature provides a visual and intuitive way to analyze how the energy of a transition state may affect a whole reaction network, to quickly explore combinations of concentrations, etc.


## Dependencies
- Numpy (>= 1.17.3)
- NetworkX (>= 2.4)
- Matplotlib (>= 3.1.2)
- Panel (>= 0.9.5) for interactive plotting

## Basic installation guide
A basic installer from setuptools, `setup.py` is provided. Inside the main folder, run:

```
pip install -e .
```

to install the package in *editable* mode: thus, all changes in the **py** files of the modules nx_reaxgraf and nx_gTOFfee will be directly applied to the calculations where the modules are used.

In this way, both packages can be imported from any Python instance on your system.
