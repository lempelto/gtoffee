import networkx as nx
import networkx.algorithms.isomorphism as iso
import numpy as np
import nx_reaxgraf as rg
import matplotlib.pyplot as plt
import copy


### Constants:
# Taken from scipy.constants: redefined here to avoid dependencies
spc_h=6.62607015e-34 #J s
spc_k=1.380649e-23 #J K-1
# Consider different units for R
R_kcal=8.314462618/4184 #kcal mol-1 K-1
R_kJ=8.314462618/1000 #kJ mol-1 K-1
J_mol_to_eV=1/(1.602176634e-19*6.02214076e+23)
R_eV=8.314462618*J_mol_to_eV

R_values = {'kcal':R_kcal,'eV':R_eV,'kJ':R_kJ}


def edge_energy_sum(graphobj,temp,energy_unit='kcal',do_exp=True):
    '''Get the exp(-SUM(Ti)/RT) corresponding to a graph object (extended
    to all its edges)
    Input:
    - graphobj: Graph object, must have EnergyCorr property at all edges
    - temp: float, temperature in K
    - energy_unit. String, kcal/kJ/eV. Defines energy unit to use, default kcal/mol.
    - do_exp. Boolean. If True, perform exponentiation, else, return the exponent only.
    Output:
    - expo_term: float, exp(-SUM(Ti)/RT) extended to all edges in input graph'''
    Rvalue=R_values[energy_unit]
    all_edges=graphobj.edges.data('EnergyCorr')
    all_energies=[item[2] for item in all_edges]
    all_edges_sum=np.sum(all_energies)
    exponent=-all_edges_sum/(Rvalue*temp)
    if (do_exp == True):
        expo_term=np.exp(exponent)
        return expo_term
    else:
        return exponent

def eff_E_span_calc(TOF,temp,out_units='kcal'):
    '''Compute the effective energy span from a TOF: rescaling from inverse
    time units to energy units
    Input:
    - TOF: float, TOF value in s-1
    - temp: float, temperature in K
    - out_units: string, kcal/kJ/eV. Specifies units for the output.
    Output: 
    - eff_E: float, eff. energy span in requested units (kcal/mol by default)'''
    Rvalue=R_values[out_units]
    logterm=spc_h*TOF/(spc_k*temp)
    eff_E=-Rvalue*temp*np.log(logterm)
    return eff_E

def semi_standard_calculator(mech,temp,dict_conc,energy_unit='kcal'):
    '''Compute semi-standard energies for a mechanism.
    Timeline and reac/prod timeline must have been already
    determined. Adds RT ln X to all energies, with X being 
    the product of all reac. concs. after the entry and all
    prod. concentrations before the entry.
    Input:
    - mech. Graph object for a reaction mechanism (all connected, one only
    cycle, with pre-computed timeline (have 'EdgesAfter', 'EdgesBefore', 'ReacsAfter'
    and 'ProdsBefore' properties).
    - temp. Float, temperature in K
    - dict_conc. Dictionary mapping reactant/product str name tags to their float
    concentration in solution in mol/L
    - energy_unit. String, kcal/kJ/eV. Defines energy unit to use, default kcal/mol.
    Output:
    - None
    - Fills the 'EnergyCorr' property of all nodes and edges in mech'''
    
    Rvalue=R_values[energy_unit]
    RT=Rvalue*temp
    #Go through nodes
    for node in mech.nodes(data=True):
        rx_node=node[1]['ReacsAfter']
        px_node=node[1]['ProdsBefore']
        Rc_vals=[dict_conc[rx] for rx in rx_node if rx != 'x']
        Pc_vals=[dict_conc[px] for px in px_node if px != 'x']
        Rconc=np.prod(Rc_vals)
        Pconc=np.prod(Pc_vals)
        G_or=node[1]['energy']
        G_semistd=G_or+RT*np.log(Rconc*Pconc)
        node[1]['EnergyCorr']=G_semistd
    #Go through edges
    for edge in mech.edges(data=True):
        rx_edge=edge[2]['ReacsAfter']
        px_edge=edge[2]['ProdsBefore']
        Rc_vals=[dict_conc[rx] for rx in rx_edge if rx != 'x']
        Pc_vals=[dict_conc[px] for px in px_edge if px != 'x']
        Rconc=np.prod(Rc_vals)
        Pconc=np.prod(Pc_vals)
        G_or=edge[2]['energy']
        G_semistd=G_or+RT*np.log(Rconc*Pconc)
        edge[2]['EnergyCorr']=G_semistd
    return None

def set_default_energy(mech):
    '''For runs without concentrations, set the EnergyCorr property
    of every node and edge as equal to the input Gibbs free energy 
    (as 'energy' property).
    Input: 
    - mech. Graph object for a reaction mechanism (all connected, one only
    cycle). Though here the timeline is not required, the 'EdgesAfter' and 
    'EdgesBefore' properties should be present for further computation.
    Output:
    - None.
    - Fills the 'EnergyCorr' property of all nodes and edges in mech. '''
    
    for node in mech.nodes(data=True):
        node[1]['EnergyCorr']=node[1]['energy']
    for edge in mech.edges(data=True):
        edge[2]['EnergyCorr']=edge[2]['energy']
    return None

def Greac_assignment(mech):
    '''Assignment of Greaction to a given mechanism, considering the 
    'EnergyCorr' properties and the predefined 'GRecipe' graph property
    to compute G.
    Input:
    - mech. Graph object for a reaction mechanism (all connected, one only
    cycle).
    Output:
    - None.
    - Fills the 'Greaction' property of the mech Graph. '''
    recipe=mech.graph['GRecipe']
    #Grecipe is a simple addition/substraction formula which allows to consider
    #cases where more than a possible closing edge is traversed.
    Greac=0
    for entry in recipe:
        sign=entry[0]
        edgestr=entry[1:]
        edgeobj=rg.string_to_obj(mech,edgestr)
        G_edge=rg.energy_from_string(mech,edgestr,'EnergyCorr')
        if (sign == '+'):
            Greac=Greac+G_edge
        else:
            Greac=Greac-G_edge
        #should add something in case the order has changed!
    mech.graph['Greaction']=Greac
    return None

def TOF_multimech(mech_container,temp):
    '''Applies the main TOF computation equation, computing the exponential
    sums for spanning trees and mechanisms.
    When treating trees, the corresponding exp(-Ij+delta) is computed, assigning
    the delta term as +Gr or 0 according to the matched mechanism (Gr if the node/edge
    is after the first edge that closes the tree to the mechanism, 0 elsewhere)
    Input:
    - mech_container. MechContainer object containing SpanTrees (list of spanning trees)
    and a MechList whose items already contain 'EdgesAfter', 'EdgesBefore', 'EnergyCorr'
    and 'Greaction
    - temp. float, temperature in K.
    Output: 
    - TOF_mechanisms. List of float TOF values in s-1 for every mechanism in the
    container.'''

    Rvalue=R_values[mech_container.EUnits]
    RT=Rvalue*temp

    Ntrees=len(mech_container.SpanTrees)
    tau_array=np.zeros((Ntrees,1))
    int_term_array=np.zeros((Ntrees,1))

    for it,st in enumerate(mech_container.SpanTrees):
        tau_i=edge_energy_sum(st,temp,energy_unit=mech_container.EUnits,do_exp=False)
        #-Ij + delta
        ref_edge=st.graph['FirstMechEdge']
        ref_edge_rev=(ref_edge[1],ref_edge[0])
        #we need the EdgesBefore property of all nodes. If the reference edge
        #is there, the node must be AFTER the edge, and we will assign d=Greac
        #0 elsewhere (node BEFORE) closing edge
        #get the associated mechanism
        mech_from_tree=st.graph['MatchedMech']
        info_nodes=mech_from_tree.nodes(data=True)
        exp_Ij_sum=0
        alt_sum=0
        for entry in info_nodes:
            enrg=entry[1]['EnergyCorr']
            edges_prev=entry[1]['EdgesBefore']
            int_after=(ref_edge in edges_prev) or (ref_edge_rev in edges_prev)
            #assign delta!
            Gr=(st.graph['MatchedMech']).graph['Greaction']
            if (int_after):
                delta=Gr
            else:
                delta=0
            #so we proceed to compute the exp((-Ij+d)/RT) term and add it to the sum
            expo_term=np.exp((-enrg+delta)/RT)
            exp_Ij_sum=exp_Ij_sum+expo_term

        #assign to arrays
        tau_array[it,0]=tau_i
        int_term_array[it,0]=exp_Ij_sum
    ###Summation of the product of these two provides the denominator!!
    #Every term in the sum is a tau_k*SUM_INT factor, only that the SUM_INT factor
    #is not unique, but tree-depending
    #if we use exponents for better stability, we shall not sum yet
#    prod_array=tau_array*int_term_array
#    kin_resist=np.sum(prod_array)

    ###Go through all mechanisms
    TOF_mechanisms=[]
    for im,mech in enumerate(mech_container.MechList):
        exp_mech=edge_energy_sum(mech,temp,energy_unit=mech_container.EUnits,do_exp=False)
        Gr_mech=mech.graph['Greaction']
        driving_force=1-np.exp(Gr_mech/RT)
        #we can substract exp_mech from every term in tau_array, and then
        #do the exponentiation with a much smaller number
        tau_div_mu_expo=tau_array-exp_mech
        kin_resist_arr=np.exp(tau_div_mu_expo)*int_term_array
        kin_resist=np.sum(kin_resist_arr)
        TOF_mech=(spc_k*temp/spc_h)*driving_force/kin_resist
        TOF_mechanisms.append(TOF_mech)
    return TOF_mechanisms

def printstatus(instring,verbosity):
    '''Printing helper allowing to shutdown messages and get less verbose
    output.
    Input:
    - instring. String to be printed.
    - verbosity. Boolean, only if True the string is printed.
    Output:
    - None.
    - Prints instring through STDOUT'''
    if (verbosity == True):
        print(instring)
    return None            
    
def TOF_wrapper(mech_container,temp,use_conc=False,dict_conc={},verbose=True):
    '''System preparation and TOF calculation for a set of valid mechanisms.
    Input:
    - mech_container. MechContainer object, only initialized: contains main graph
    and generated mechanisms with the 'energy' property (and for edges, also
    'edgestring'). All other properties will be included throughout this function.
    - temp. Float, temperature in K.
    - use_conc. Boolean, use concentration if True, else assign 'EnergyCorr' = 'energy'
    - dict_conc. Dictionary mapping reactant/product str name tags to their float
    concentration in solution in mol/L.
    - verbose. Boolean, if False status output is turned off.
    Output:
    - tofs: list of float TOF values in s-1.
    - dE_vals: list of float eff. energy spans in kcal/mol.
    - overall_tof: float, summation of all TOFs in s-1.
    - overall_dE: float, eff. energy span computed from overall_tof
    - Many properties of the individual mechanisms in the container are added
    throughout the function.
     '''
    printstatus("Building directed graphs and timeline...",verbose)
    mechUlist=[]
    for im,mech in enumerate(mech_container.MechList):
        try:
            mechD=rg.graph_director(mech)
            mech_container.DirMechList.append(mechD)
            mechU=rg.timeline(mechD)
            mechUlist.append(mechU)
        except: 
            print("The graph for mechanism. %d could not be traversed" % im)
            print("Please check input: be sure that closing edges are properly defined")
            print("Closing edge in failed mechanism:",mech.graph['ClosingEdge'])
            closer_degree=mech.degree(mech.graph['ClosingEdge'][0])
            if (closer_degree != 2):
                print("="*42)
                print("Last node of a mechanism must have only two connections!")
                print("Here, %s has %d connections" % (mech.graph['ClosingEdge'][0],closer_degree))
                print("Please adapt input network to the required format")
            raise SystemExit("Found problem at mech. %d" % im)
    mech_container.MechList=mechUlist
    if (use_conc == True):
        printstatus("Computing semi-standard energies",verbose)
        [semi_standard_calculator(mech,temp,dict_conc,mech_container.EUnits) for mech in mech_container.MechList]
    else:
        printstatus("No conc. effects: use input energies",verbose)
        [set_default_energy(mech) for mech in mech_container.MechList]
    printstatus("Assigning reaction energies...",verbose)
    [Greac_assignment(mech) for mech in mech_container.MechList]
    printstatus("Building spanning trees...",verbose)
    rg.span_tree_generator(mech_container,verbose)
    rg.span_tree_closer(mech_container,verbose)
    Ntrees=len(mech_container.SpanTrees)
    Ntree_string="Constructed %d trees" % Ntrees
    printstatus(Ntree_string,verbose)
    printstatus("Running TOF calculation!",verbose)
    tofs=TOF_multimech(mech_container,temp)
    dE_vals=[eff_E_span_calc(tof,temp,mech_container.EUnits) for tof in tofs]
    overall_tof=np.sum(tofs)
    overall_dE=eff_E_span_calc(overall_tof,temp,mech_container.EUnits)
    return tofs,dE_vals,overall_tof,overall_dE
    
def TOF_wrapper_preinit(mech_container,temp,use_conc=False,dict_conc={},verbose=True):
    '''System preparation and TOF calculation for a set of already prepared valid mechanisms
    (-> trees shall already have been generated and matched to mechanisms).
    Modify semi-standard energies.
    Input:
    - mech_container. MechContainer object, fully prepared: contains main graph
    and generated mechanisms and spanning trees, with all traversal-related properties.
    EnergyCorr will be changed according to use_conc/dict_conc
    - temp. Float, temperature in K.
    - use_conc. Boolean, use concentration if True, else assign 'EnergyCorr' = 'energy'
    - dict_conc. Dictionary mapping reactant/product str name tags to their float
    concentration in solution in mol/L.
    - verbose. Boolean, if False status output is turned off.
    Output:
    - tofs: list of float TOF values in s-1.
    - dE_vals: list of float eff. energy spans in kcal/mol.
    - overall_tof: float, summation of all TOFs in s-1.
    - overall_dE: float, eff. energy span computed from overall_tof
     '''
    printstatus("Using pre-existing mechanisms, trees and timeline",verbose)
    if (use_conc == True):
        printstatus("Computing semi-standard energies",verbose)
        [semi_standard_calculator(mech,temp,dict_conc,mech_container.EUnits) for mech in mech_container.MechList]
        [semi_standard_calculator(tree,temp,dict_conc,mech_container.EUnits) for tree in mech_container.SpanTrees]
    else:
        printstatus("No conc. effects: use input energies",verbose)
        [set_default_energy(mech) for mech in mech_container.MechList]
        [set_default_energy(tree) for tree in mech_container.SpanTrees]
    printstatus("Assigning reaction energies...",verbose)
    [Greac_assignment(mech) for mech in mech_container.MechList]
    Ntrees=len(mech_container.SpanTrees)
    Ntree_string="Using %d trees" % Ntrees
    printstatus(Ntree_string,verbose)
    printstatus("Running TOF calculation!",verbose)
    tofs=TOF_multimech(mech_container,temp)
    dE_vals=[eff_E_span_calc(tof,temp,mech_container.EUnits) for tof in tofs]
    overall_tof=np.sum(tofs)
    overall_dE=eff_E_span_calc(overall_tof,temp,mech_container.EUnits)
    return tofs,dE_vals,overall_tof,overall_dE    
    
def mech_init_store(mech_container,temp,verbose=True,outfile="binary_network.bin"):
    '''System preparation for a set of valid mechanisms, further stored as a binary file.
    Input:
    - mech_container. MechContainer object, only initialized: contains main graph
    and generated mechanisms with the 'energy' property (and for edges, also
    'edgestring'). All other properties will be included throughout this function.
    - temp. Float, temperature in K.
    - verbose. Boolean, if False status output is turned off.
    Output:
    - tofs: list of float TOF values in s-1.
    - dE_vals: list of float eff. energy spans in kcal/mol.
    - overall_tof: float, summation of all TOFs in s-1.
    - overall_dE: float, eff. energy span computed from overall_tof
    - Many properties of the individual mechanisms in the container are added
    throughout the function.
     '''
    use_conc=False
    dict_conc={}
    #do a simple run without concentration effects
    printstatus("Building directed graphs and timeline...",verbose)
    mechUlist=[]
    for im,mech in enumerate(mech_container.MechList):
        mechD=rg.graph_director(mech)
        mech_container.DirMechList.append(mechD)
        mechU=rg.timeline(mechD)
        mechUlist.append(mechU)
    mech_container.MechList=mechUlist
    #no concentration effects
    printstatus("No conc. effects: use input energies",verbose)
    [set_default_energy(mech) for mech in mech_container.MechList]
    printstatus("Assigning reaction energies...",verbose)
    [Greac_assignment(mech) for mech in mech_container.MechList]
    printstatus("Building spanning trees...",verbose)
    rg.span_tree_generator(mech_container)
    rg.span_tree_closer(mech_container)
    Ntrees=len(mech_container.SpanTrees)
    Ntree_string="Constructed %d trees" % Ntrees
    printstatus(Ntree_string,verbose)
    printstatus("Running test TOF calculation!",verbose)
    #test run for TOFs to check that everything works
    try:
        tofs=TOF_multimech(mech_container,temp)
    except:
        print("TOFs could not be computed!")
    #save to file
    rg.binary_savefile(mech_container,outfile)
    printstatus("Successfully saved container as %s" % outfile,verbose)
    return tofs
    
    
###Tools for auto-testing of concentrations

def conc_iterator_2D(list_entries):
    '''Generation of 2-concentration value sets for testing.
    Input: 
    - list_entries. List of lists, containing LAB,c0,cF,Nelems
    for each species whose concentration is being tested.
    Output: 
    - Carr. np.array of 2-tuples with floats corresponding to the
    concentrations that are changing'''
    #Build the array (2D)
    splist=[entry[0] for entry in list_entries]
    c1data=list_entries[0][1:]
    c2data=list_entries[1][1:]
    c1=np.linspace(*c1data)
    c2=np.linspace(*c2data)
    Carr=np.zeros((len(c1),len(c2)),dtype=object)
    #c1 changes across rows, and c2 across columns (fastest-changing dist)
    for i,ci in enumerate(c1):
        for j,cj in enumerate(c2):
            Carr[i,j]=(ci,cj)
    return Carr

def dict_prep(template,Cvals,species):
    '''For a template dictionary, modify the requested concentrations.
    Input: 
    - template. Dictionary to be modified, with str:float pairs (tag:concentration in mol/L). 
    The entries that are not being modified will remain constant. Deep-copied at every iteration
    due to mutability.
    - Cvals. Tuple of floats with the concentrations in mol/L to be tested.
    - species. List of strings with the tags of the species whose concentrations
    are tested: must be ordered just like in Cvals
    Output:
    - dictx: Dictionary with the requested concentrations (float, in mol/L) for the changing species and
    template values for the rest.
    '''
    dictx=copy.deepcopy(template)
    for ii,sp in enumerate(species):
        cx=Cvals[ii]
        dictx[sp]=cx
    return dictx

def TOF_conc_test(mech_container,temp,Carr,list_entries,processes,template_dict,pre_init_model=False):
    '''TOF computation across a set of concentrations. 
    Designed for cases with 2 different products, with previous knowledge of which
    mechanisms correspond to which product.
    Input:
    - mech_container. MechContainer object with all mechanisms. Will be updated
    at every iteration, can be only initialized at input.
    - temp. Float, temperature in K
    - Carr. np.array of 2-tuples of concentrations (float, in kcal/mol) 
    - list_entries. List of lists of the form SPECIES, c0, c_end, Nentries. 
    - processes. List of lists for indices of mechanisms leading to one or other product.
    If we had only one reaction route and one product, just consider all indices in one list
    and leave an arbitrary one for the second, as a dummy.
    - template_dict. Dictionary to be modified, with str:float pairs (tag:concentration in mol/L). 
    The entries that are not being modified will remain constant. Deep-copied at every iteration
    due to mutability.
    
    Output: 
    - results. List of numpy arrays with TOFs (float, in s-1) for process 1 and process 2'''
    r1arr=np.zeros_like(Carr)
    r2arr=np.zeros_like(Carr)
    splist=[entry[0] for entry in list_entries]
    results=[]
    ic=0
    for ii in range(Carr.shape[0]):
        for jj in range(Carr.shape[1]):
            #Columns are traversed first: the second species changes along columns and the
            #first along rows
            Cset=Carr[ii,jj]
            c_now=dict_prep(template_dict,Cset,splist)
            #Run the TOF calculation for the current data
            if (pre_init_model == False):
                A,B,C,D=TOF_wrapper(mech_container,temp,True,c_now,verbose=False)
            else:
                A,B,C,D=TOF_wrapper_preinit(mech_container,temp,True,c_now,verbose=False)
                        #use information of products
            TOFS_1=np.array(A)[processes[0]]
            TOFS_2=np.array(A)[processes[1]]
            TOF_prod1=np.sum(TOFS_1)
            TOF_prod2=np.sum(TOFS_2)
            r1arr[ii,jj]=TOF_prod1
            r2arr[ii,jj]=TOF_prod2
            #print("Iter. %d, (%d,%d), [%s]=%6.2f [%s]=%6.2f" % (ic,ii,jj,splist[0],Cset[0],splist[1],Cset[1]))
            if (ic % 25 == 0):
                print("="*24,"Iteration %d" % ic,"="*24)
            #counter
            ic=ic+1
    results=[r1arr,r2arr]
    return results



def plot_matrix_quotient(results_array,list_entries):
    '''Get a map for the values of TOF_1/TOF_2 across an array, corresponding
    to different concentration values
    Input: 
    - results_array: list of TOF arrays for 1 and 2
    - list_entries: list of lists SPECIES, c0, c_end, Nitems to know
    the tested concentration values
    Output: 
    - fig,ax. Figure and axis objects for the generated matplotlib image.'''
    Arr1=results_array[0]
    Arr2=results_array[1]
    DiffArr=(Arr1/Arr2).astype(float)
    ##regeneration of concentration lists
    c1data=list_entries[0][1:]
    c2data=list_entries[1][1:]
    c1=np.linspace(*c1data)
    c2=np.linspace(*c2data)
    splist=[entry[0] for entry in list_entries]
    ##figure generation
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    #horizontal axis corresponds to c2, which changes across columns
    #vertical axis corresponds to c1, which changes across rows
    mesh=ax.imshow(DiffArr,cmap='jet',extent=[c2[0],c2[-1],c1[-1],c1[0]])

    Xlab=["%6.2f"%val for val in c2]
    Ylab=["%6.2f"%val for val in c1]

    ax.set_xlabel("[%s]/mol L-1" % splist[1])
    ax.xaxis.set_label_position('top') 
    ax.xaxis.set_ticks_position('top') 

    ax.set_ylabel("[%s]/mol L-1" % splist[0])
    cbar=plt.colorbar(mesh)
    cbar.set_label("Ratio 1-to-2")
    plt.tight_layout()
    return fig,ax

def energy_summary_typologies(mech_container,dummy_list,temp,TOF_values,dE_values):
    '''
    For a pre-judged list of mechanisms (so typologies are known), group turnover frequency
    and eff. energy span values per typology, and print a summary on the feasibility of
    each identified mech. type.
    Input:
    - mech_container. Instantiated MechanismContainer object 
    - dummy_list: List of nx.Graph object containing individual mech. typologies
    (unique catalytic cycles) obtained via mech_judge()
    - temp. Float, temperature in K.
    - TOF_values. List of floats, turnover frequencies for the current MechContainer.
    - dE_values. List of floats, effective energy spans for the current MechContainer.
    Output:
    - output_list. List of lists, containing TOF and dE values per mechanism kind, and the
    indexes for mechanism kinds (for consistency)
    - Prints to STDOUT.
    '''
    TOF_arr = np.array(TOF_values)
    dE_arr = np.array(dE_values)

    #process mechanism typologies
    mechtypes = rg.mech_typology(mech_container,dummy_list)
    #Prepare selections
    A_subsets = []
    B_subsets = []
    kind_list = []
    #print header
    print("|Type|Ntype|   Gr |dE(best)|dE(sum)| TOF(sum)")
    for kind in set(mechtypes):
        typo_loc = (mechtypes==kind)
        Ntypology = np.sum(typo_loc)
        typo_ndx_list = [item[0] for item in np.argwhere(typo_loc)]

        TOF_kind = TOF_arr[typo_loc]
        dE_kind = dE_arr[typo_loc]
        mechs_from_typo = [mech_container.MechList[ik] for ik in typo_ndx_list]
        #on-the-fly
        #smallest eff. energy span value: get index and then all properties
        best_dE_ndx = np.argmin(dE_kind)
        best_dE = dE_kind[best_dE_ndx]
        best_mech = mechs_from_typo[best_dE_ndx]
        # best_mech_tag = typo_ndx_list[best_dE_ndx] # Unused
        Gr = (best_mech.graph['Greaction'])
        #the overall TOF
        joined_TOF = np.sum(TOF_kind)
        joined_dE = (eff_E_span_calc(joined_TOF,temp,out_units=mech_container.EUnits))
        
        ###printing
        print("| %2d |  %2d |%6.2f| %6.2f |%6.2f | %8.3g" % (kind,Ntypology,Gr,best_dE,joined_dE,joined_TOF))
        #save in lists to allow further processing
        A_subsets.append(TOF_kind)
        B_subsets.append(dE_kind)
        kind_list.append(kind) ###for consistency
    output_list = [A_subsets,B_subsets,kind_list]
    return output_list
