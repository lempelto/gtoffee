from tkinter.messagebox import NO
import networkx as nx
import networkx.algorithms.isomorphism as iso
from networkx.drawing.nx_pydot import read_dot
import numpy as np
import matplotlib.pyplot as plt
import itertools
import time
import pickle

class MechanismContainer:
    '''Storage of graph objects and related properties to facilitate I/O
    across all gTOFfee functions.
    Principal attributes:
    - MainGraph. Graph object describing the full reaction network.
    - StartNode. String, label for the reference (initial) node of the cycle.
    - MechList. List for Graph objects for each individual reaction mechanism
    (one-cycle subgraphs, with all nodes, of the network.
    - DirMechList. List for DiGraph objects for each individual reaction mechanism
    (from StartNode to end of cycle and to all branches).
    - SpanTrees. List for Graph objects for each spanning tree in the network
    (subgraphs without cycles, but connecting all nodes).
    - Nmechanisms. Integer, number of mechs (length of MechList)
    - EUnits. String, units for energy (kcal/kJ/eV). By default, uses kcal -> kcal/mol.
    For kJ or eV, change this property manually after generating the mechanisms.
    - PosMainGraph. Layout of MainGraph, via NetworkX.spring_layout()
    for further plotting'''
    def __init__(self):
        self.MainGraph=None
        self.StartNode=None
        self.MechList=[]
        self.DirMechList=[]
        self.SpanTrees=[]
        self.Nmechanisms=0
        self.EUnits='kcal'
    def node_layout_gen(self):
        self.PosMainGraph=nx.spring_layout(self.MainGraph)

def nodereader(node_input):
    '''Read the data file containing node information.
    ==File structure==
    Label    Energy    Connections
    Int1       I1         Int2, ...
    Int2       I2        Int1,Int3
    ...       ...           ...
    IntK       Ik           ...
    Input:
    - node_input. String, file to read node data from.
    Output:
    - labels. List of string node labels.
    - energies. List of float node energies in kcal/mol.
    - connections_list. List of string 2-tuples stating node connectivity.
    '''
    with open(node_input) as fdat:
        read_obj=fdat.readlines()
    lines_obj=[entry.split() for entry in read_obj]
    labels=[line[0] for line in lines_obj]
    energies=[float(line[1]) for line in lines_obj]
    connections=[line[2] for line in lines_obj] 
    connections_list=[item.split(',') for item in connections]
    return labels,energies,connections_list

def edgereader(edge_input):
    '''Read the data file containing edge information
    ==File structure==
    LABEL   ENERGY (TS)   REAC     PROD
    1_2         T12       R_12     P_12
    2_3         T23       R_23     P_33
    ...
    (k-1)_k     T(k-1)k   R_(k-1)k P_(k-1)k
    There is a check to find if these edges match with the
    connections in the ReactionGraph
    Input:
    - edge_input. String, file to read edge data from.
    Output:
    - edge_list. List of strings "node1_node2" defining the edges of the mechanism.
    Input direction is only meaningful in terms of matching reactants and products: 
    will be changed if needed to form mechanisms.
    - ts_energies. List of floats with the energy of every TS in kcal/mol.
    - reacs_list. List of strings with reactants that enter the network in a given edge.
    - prods_list. List of strings with products that exit the network in a given edge.
    (This is according to the input direction: if the edge is really traversed as node2-to-node1
    reacs and prods will be inverted accordingly)
    '''
    with open(edge_input) as fedge:
        read_obj=fedge.readlines()

    lines_obj=[entry.split() for entry in read_obj]
    edge_list=np.array([line[0] for line in lines_obj])
    ts_energies=[float(line[1]) for line in lines_obj]
    reacs_list=[line[2].split(',') for line in lines_obj]
    prods_list=[line[3].split(',') for line in lines_obj]
    # process lists of reactants and products
    return edge_list,ts_energies,reacs_list,prods_list
   
def tuple_to_string(edge_tuple):
    '''Convert a 2-string tuple (node1,node2) into a
    string node1_node2.
    Input: 
    - edge_tuple. 2-tuple of strings with node tags.
    Output:
    - String with node tags separated by underscore.'''
    return edge_tuple[0]+"_"+edge_tuple[1]
    
def string_to_tuple(edge_string):
    '''Convert a string node1_node2 into a 2-tuple of
    strings (node1,node2)
    Input:
    - edge_string. String with node tags sep. by underscore
    Output:
    - 2-tuple of strings with node tags.'''
    list_repr=edge_string.split("_")
    return tuple(list_repr)
    
def string_to_obj(graph_obj,edge_string):
    '''Convert a string node1_node2 into an edge
    object taken from the input graph
    Input:
    - graph_obj. Graph object describing a network/mechanism/tree.
    - edge_string. String with node tags sep. by underscore
    Output:
    - EdgeView object from graph_obj after fetching the tuple generated
    by edge_string, with all edge properties accessible'''
    tuple_repr=string_to_tuple(edge_string)
    return graph_obj[tuple_repr[0]][tuple_repr[1]]
    
def edge_string_invert(edge_string):
    '''Transform a node1_node2 string defining an edge into the
    node2_node1 form corresponding to the reverse direction
    Input:
    - edge_string. String with node tags sep. by underscore.
    Output:
    - String with reversed node tas sep. by underscore.'''
    tuple_repr=string_to_tuple(edge_string)
    return tuple_repr[1]+"_"+tuple_repr[0]

def edge_assessor(nodefile):
    '''For a file with nodes and connections, generate all unique
    edges as edgestrings to facilitate edge file setup -> be careful
    with the direction of the edgestring
    Input:
    - nodefile. String, file with node data.
    Output:
    - None.
    - Print edge names as STDIN'''
    nodenames,elist,connections=nodereader(nodefile)
    all_edges=[]
    print("="*10,"Edge names from connectivity","="*10)
    for ii,node in enumerate(nodenames):
        current_edges=[(node,entry) for entry in connections[ii]]
        for pair in current_edges:
            dir_string=pair[0]+"_"+pair[1]
            rev_string=pair[1]+"_"+pair[0]
            if ((dir_string not in all_edges) and (rev_string not in all_edges)):
                print(dir_string)
                all_edges.append(dir_string)
    print("="*42)
    return None

def graph_instantiator(nodefile,edgefile,closing_edges):
    '''Convert node and edge files into a NetworkX Graph object,
    including energies, connections, reactants and products. Wraps
    the nodereader() and edgereader() functions. Defines the reaction
    network.
    Input:
    - nodefile. String, file to take node data from.
    - edgefile. String, file to take edge data from.
    - closing_edges. List of strings referring to the edges that
    can close a catalytic cycle in the main network.
    Output:
    - G. Initialized nx.Graph object for the basic reaction network
    (with energies, connections and reactant/products for each edge)'''
    G=nx.Graph()
    labs,enrgs,connects=nodereader(nodefile)
    edges,ts,rxlist,pxlist=edgereader(edgefile)
    for ii,node in enumerate(labs):
        e=enrgs[ii]
        G.add_node(node,energy=e,name=node)
        [G.add_edge(node,partner) for partner in connects[ii]]
    # Test whether edges in edgefile are defined in the connections
    checklist=[]
    for ie,edge in enumerate(edges):
        pair=edge.split('_')
        edge_flag=G.has_edge(pair[0],pair[1])
        checklist.append(edge_flag)
        if (edge_flag == True):
            G.edges[pair[0],pair[1]]['energy']=ts[ie]
            G.edges[pair[0],pair[1]]['reacs']=rxlist[ie]
            G.edges[pair[0],pair[1]]['prods']=pxlist[ie]
            edgestring=tuple_to_string(tuple([pair[0],pair[1]]))
            G.edges[pair[0],pair[1]]['edgestring']=edgestring
    #add the closing_edge property to the graph
    G.graph['ClosingEdgesList']=closing_edges
    # Return None if edges are not well defined
    if (np.all(checklist) == False):
        print("INPUT ERROR\n","="*42)
        print("Check input. Some edges defined in the edgefile ARE NOT connections in the nodefile")
        print("Non-defined edges:")
        culprits=[ed for ie,ed in enumerate(edges) if (checklist[ie] == False)]
        print(culprits)
        print("="*42)
        return None

    # Test whether all edges defined in the connections have been defined also in edgefile
    # looking for energy in the EdgeView: if there is a None, abort
    undefined_edges=[(ed[2] == None) for ed in G.edges(data='energy')]
    if (np.any(undefined_edges) == True):
        print("INPUT ERROR\n","="*42)
        print("Check input. Some connections in the nodefile ARE NOT defined in the edgefile.")
        print("Non-defined edges:")
        culprits=[ed for ie,ed in enumerate(G.edges) if (undefined_edges[ie] == True)]
        print(culprits)
        print("="*42)
        return None

    # Check for exergonic character in all closing edges
    e_closers = [energy_from_string(G,closer) for closer in closing_edges]
    exergonic_paths = [e < 0.0 for e in e_closers]
    if (np.all(exergonic_paths) == False):
        print("INPUT ERROR\n","="*42)
        print("There are closing edges that are not exergonic")
        print("so TOF calculation is not appliable")
        print("Please correct your input to compute the TOF")
        print("(Graph object is still returned)")
    return G

def energy_from_string(mech,edge_string,req_string='energy'):
    '''Fetch the energy of a given edge_string, with either the
    'energy' or the 'EnergyCorr' keywords.
    Input:
    - mech. nx.Graph object for the requested mechanism.
    - edge_string. String defining a edge, sep. by underscore.
    - req_string. String, property to check for the edge. Either
    'energy' or 'EnergyCorr', if semi-std. energies are available
    in mech
    Output: 
    - e_val. Float, energy assorted to the input edgestring. '''
    edge_obj=string_to_obj(mech,edge_string)
    e_val=edge_obj[req_string]
    return e_val
    
        
def mechanism_generator(mech_trial,edge_out):
    '''Check whether removing a given edge from the main graph generates 
    a new valid mechanism (unique, one-and-only cycle subgraph connecting
    all nodes)
    Input:
    - mech_trial. nx.Graph object to generate a mechanism from. Shall be a
    deep copy of the original reaction network nx.Graph.
    - edge_out. 2-tuple of strings with node tags corresponding to the edge
    which is being removed.
    Output:
    - discard_mech. Boolean, if True the mechanism is not valid and must be discarded.
    - mech_trial object is updated in-place, including the 'GRecipe', 'ClosingEdge' and
    'MainCatCycle' properties'''
    mech_trial.remove_edges_from(edge_out)
    #check whether all nodes are connected
    connect_flag=nx.is_connected(mech_trial)
    if (connect_flag == False):
        discard_mech=True
        return discard_mech
    
    #do not use start_node as parameter!
    cycle=nx.find_cycle(mech_trial)
    
    #For the main catalytic cycle, consider the two directions in which it can be traversed
    #just like in nx.find_cycle output, or inverted. Use edgestrings for directionality.
    dir_cycle_string=[tuple_to_string(edgetuple) for edgetuple in cycle]
    rev_cycle_string=[edge_string_invert(edgestr) for edgestr in dir_cycle_string][::-1]
    #Now determine whether possible closing edges are in each of these directionalized cycles
    dir_closure_list=[closer in dir_cycle_string for closer in mech_trial.graph['ClosingEdgesList']]
    rev_closure_list=[closer in rev_cycle_string for closer in mech_trial.graph['ClosingEdgesList']]
    #now we have bool lists mapping whether a proper closing edge is in a single cat. cycle
    
    #Join in lists to use indexing further on
    dir_rev_cycles=np.array([dir_cycle_string,rev_cycle_string])
    dir_rev_closures=[dir_closure_list,rev_closure_list]
    
    dir_rev_flags=[np.any(closelist) for closelist in dir_rev_closures]
    Nmatches=np.sum(dir_rev_flags)

    #The value of Nmatches maps the situations that can be encountered
    #0: no valid cycle. Skip.
    #1: a cycle closed with a suitable closing edge is present. Simple assignment.
    #2: both directions contain suitable closing edges. Evaluate which is most exergonic.
    
    dir_closure_flag=np.any(dir_closure_list)
    rev_closure_flag=np.any(rev_closure_list)
    
    if (Nmatches == 0):
        discard_mech=True
        return discard_mech
        
    elif (Nmatches == 1):
        #check whether forward or backward direction contain the closing edge
        discard_mech=False
        #take the index of the direction where the flag is True
        ndx_use=np.argwhere(dir_rev_flags)[0][0]
        #will be 0 for forward (direct) and 1 for backward (reverse)
        cycle_string=dir_rev_cycles[ndx_use]
        closure_list=dir_rev_closures[ndx_use]
        current_closer=np.array(mech_trial.graph['ClosingEdgesList'])[closure_list]
        mech_trial.graph['GRecipe']=["+"+current_closer[0]]
        Greac=energy_from_string(mech_trial,current_closer[0])
    else:
        ###both directions contain closing edges: must decide which is most exergonic
        discard_mech=False
        #select which is the closer for each case
        dir_closer=np.array(mech_trial.graph['ClosingEdgesList'])[dir_rev_closures[0]]
        rev_closer=np.array(mech_trial.graph['ClosingEdgesList'])[dir_rev_closures[1]]
        both_closers=[dir_closer[0],rev_closer[0]]
        #obtain the free energies of the involved edges
        Gvals=[energy_from_string(mech_trial,closer) for closer in both_closers]
        minGndx=np.argmin(Gvals)
        maxGndx=np.argmax(Gvals)
        cycle_string=dir_rev_cycles[minGndx]
        closure_list=dir_rev_closures[minGndx]
        Grecipe=["+"+both_closers[minGndx]]+["-"+both_closers[maxGndx]]
        mech_trial.graph['GRecipe']=Grecipe
        
    #identify the closing edge of the mechanism
    current_closer=np.array(mech_trial.graph['ClosingEdgesList'])[closure_list]
    closer_tuple=string_to_tuple(current_closer[0])
    mech_trial.graph['ClosingEdge']=closer_tuple
    mech_trial.graph['MainCatCycle']=cycle
    
    return discard_mech
        
def mechanism_fetcher(GraphNX,start_node=None,protected_edges=[]):
    '''Iterative edge removal along a reaction network to generate all possible mechanisms. Acts as a 
    wrapper for mechanism_generator() to construct and check every mech. To handle increasingly complex reaction
    networks that can have Nc cycles, removes Nc-1 edges at a time to build the subgraphs.
    Also considers possible isomorphisms between each new mechanism and those that have already been accepted
    to check uniqueness.
    Input:
    - GraphNX. nx.Graph object for the reaction network.
    - start_node. Placeholder for compatibility with old version: not used.
    - protected_edges. List of strings corresponding to edges that should not be
    removed throughout the process.
    Output:
    - mechs. MechanismContainer object with the list of formed mechanisms (MechList)
    and the initial graph (MainGraph)'''
    ##initialize the container
    mechs=MechanismContainer()
    mechs.MainGraph=GraphNX
    all_edges=GraphNX.edges
    main_nodelist=GraphNX.nodes
    mech_mat_list=[]
    #-> obtain position of main graph for further plotting
    mechs.node_layout_gen()
    test_mechanisms=[]
    #this function will be used to check isomorphism
    node_match_func=iso.categorical_node_match(attr="name",default="XXX")
    #now generate the required combinations: first find no. of cycles in orig. graph
    Ncyc_network=len(nx.cycle_basis(GraphNX))
    print(Ncyc_network,"cycles in the main network")
    #we must remove Ncyc-1 edges at a time to build valid mechanisms
    edge_combinations=list(itertools.combinations(all_edges,Ncyc_network-1))
    print(len(edge_combinations),"combinations will be tested")
    for ie,edge_comb in enumerate(edge_combinations):
        string_repr=[tuple_to_string(edge) for edge in edge_comb]
        #Don't go on for protected edges
        protect_flag=[item in protected_edges for item in string_repr]
        if np.any(protect_flag):
            continue
        mech_trial=GraphNX.copy()    
        discard_flag=mechanism_generator(mech_trial,edge_comb)
        if (discard_flag == True):
            continue
        
        #append to mechanism list if the graph is new: check isomorphism including the NAME attribute
        #improved isomorphism check from matrices
        # mech_mat=nx.to_numpy_matrix(mech_trial)
        mech_mat=nx.to_numpy_array(mech_trial)
        # isomorph_check=[nx.is_isomorphic(mech_trial,prev,node_match_func) for prev in test_mechanisms]
        # isomorph_check=[(mech_mat==mat).all() for mat in mech_mat_list]
        if len(mech_mat_list) != 0:
            ic_tmp = mech_mat == mech_mat_list
            isomorph_check = np.all(ic_tmp,(1,2))
            isomorph_flag=np.any(isomorph_check)
        else: isomorph_flag = False

        if (isomorph_flag == False):
            mech_mat_list.append(mech_mat)
            test_mechanisms.append(mech_trial)
            mech_trial.graph['RemovedEdge']=edge_comb
            mech_trial.graph['MainLayout']=mechs.PosMainGraph
            #assign start node depending on closing edge!
            mech_trial.graph['StartNode']=mech_trial.graph['ClosingEdge'][1]
        else:
            print("Discarding pre-existing mechanism!")
        mechs.MechList=test_mechanisms
        mechs.Nmechanisms=len(test_mechanisms)
        if (ie % 200 == 0):
            print("%d/%d combs. tested" % (ie,len(edge_combinations)))
    # check exergonicity
    ordered_closers = [tuple_to_string(m.graph['ClosingEdge']) for m in mechs.MechList]
    e_closers = [energy_from_string(mechs.MechList[jj],cl) for jj,cl in enumerate(ordered_closers)]
    not_exergonic = np.array([e >= 0.0 for e in e_closers])
    for im,mcheck in enumerate(not_exergonic):
        if (mcheck == True):
            print("ENDERGONICITY DETECTED\n","="*42)
            print("Mech. %d/%d shows endergonic closing edge" % (im+1,len(not_exergonic)))
    print("%d mechanisms have been accepted" % len(mechs.MechList))
    return mechs
    
def mech_judge(mech_container,gen_fig=False,figsize=(10,10),verbose_print=False):
    '''For a MechContainer object, obtain all different true catalytic cycles
    in the network (mechanism typologies) and store the corresponding edges for further analysis.
    Input: 
    - mech_container. Instantiated MechanismContainer object
    - gen_fig. Boolean, if True, plot every new mechanism typology.
    - figsize. tuple of the dimensions for the generated figure (if gen_fig == True).
    - verbose_print. Boolean, if True, print information about every typology to STDIN.
    Output:
    - dummy_graph_list. List of nx.Graph objects defined as subgraphs of the input mechanism
    without any edge in branches, only considering the in-cycle edges for each typology
    - unique_cat_cycles. List of lists of edges defining a catalytic cycle typology'''
    #isomorphism check
    node_match_func=iso.categorical_node_match(attr="name",default="XXX")
    #list initialization
    unique_cat_cycles=[]
    dummy_graph_list=[]
    cycle_Gr_list=[]
    try:
        posx = mech_container.PosMainGraph
    except:
        posx = nx.spring_layout(mech_container.MainGraph,k=0.5,iterations=150)
    unique_counter=0
    for mech in mech_container.MechList:
        cycle=mech.graph['MainCatCycle']
        Grecipe=mech.graph['GRecipe']
        if (cycle not in unique_cat_cycles):
            #further uniqueness check: isomorphism of dummy graph
            dummy=mech.edge_subgraph(cycle)
            iso_check=[nx.is_isomorphic(dummy,prev,node_match_func) for prev in dummy_graph_list]
            iso_flag=np.any(iso_check)
            #only proceed if there are NO isomorphisms
            if (iso_flag == False):
                #fill lists
                data=(cycle,Grecipe)
                unique_cat_cycles.append(cycle)
                cycle_Gr_list.append(data)
                dummy_graph_list.append(dummy)
                unique_counter=unique_counter+1
                figfile="cycle%d.png" % unique_counter
                if (gen_fig == True):
                    plt.figure(figsize=figsize)
                    nx.draw_networkx_nodes(mech,posx,node_size=150,alpha=0.7)
                    nx.draw_networkx_labels(mech,posx)
                    nx.draw_networkx_edges(mech,posx,edgelist=cycle,width=2.0,edge_color='#FF1493')
                    plt.savefig(figfile,dpi=300)
                #rank the edges and get the N maximum energies
                if (verbose_print == True):
                    #Fetch edge information and get a summary of the largest edges in the
                    #found mechanism typology. Might be separated or removed sometime.
                    print("="*21,"Cycle %d" % unique_counter,"="*21)
                    e_edges=np.array([mech[edge[0]][edge[1]]['energy'] for edge in cycle])
                    ii_sorted=np.argsort(e_edges)[::-1]
                    ndx_max=ii_sorted[0:5]
                    edges_max=[cycle[ii] for ii in ndx_max]
                    e_vals_max=e_edges[ndx_max]
                    for edge,e_edge in zip(edges_max,e_vals_max):
                        print("%s: %6.2f kcal/mol" % (edge,e_edge))
    return dummy_graph_list,unique_cat_cycles

def mech_typology(mech_container,dummy_list):
    '''For a judged list of mechanism typologies, assign a property to every
    one corresponding to the mech. type label.
    Input:
    - mech_container. Instantiated MechanismContainer object 
    - dummy_list: List of nx.Graph object containing individual mech. typologies
    (unique catalytic cycles) obtained via mech_judge()
    Output:
    - out_typo. Numpy array with typology indices for all mechanisms in list
    - Fills the MechTypology property of every mechanism'''
    #Function for isomorphism check
    node_match_func=iso.categorical_node_match(attr="name",default="XXX")
    out_typo_list=[]
    for a_mech in mech_container.MechList:
        current_cycle=a_mech.graph['MainCatCycle']
        current_dummy=a_mech.edge_subgraph(current_cycle)
        iso_check=[nx.is_isomorphic(current_dummy,ref,node_match_func) for ref in dummy_list]
        type_ndx=np.argwhere(iso_check)[0][0] 
        a_mech.graph['MechTypology']=type_ndx
        out_typo_list.append(type_ndx)
    out_typo=np.array(out_typo_list)
    return out_typo

def mech_typology_plot(mech_container,cyc_list,grid_fig=None,plot_size=2):
    '''
    For a judged list of mechanism typologies, plot all cyclic reaction routes over
    the basic network structure.
    Input:
    - mech_container. Instantiated MechanismContainer object.
    - cyc_list. List of lists of edges defining the cyclic part of each mech. typology.
    - grid_fig. Tuple, specifying size of plt.subplots grid. If None, grid is automatically generated.
    - plot_size. Integer, figsize argument for every individual plot.
    Output:
    - fig,ax. Matplotlib figure. plt.subplots grid with all typologies.
    '''
    # Colormap preparation: use hexadecimal RGB codes to avoid problems with
    # nx.draw_networkx_edges
    import matplotlib
    cmx=matplotlib.cm.get_cmap('tab20')
    Ncolors=len(cyc_list)
    ii_colors=np.linspace(0,1,Ncolors)
    array_colors=[cmx(ii) for ii in ii_colors]
    array_colors_hex = []
    for colx in array_colors:
        rdec,gdec,bdec=colx[0:3]
        col_hex="#%02x%02x%02x" % (int(255*rdec),int(255*gdec),int(255*bdec))
        array_colors_hex.append(col_hex)

    # Generation of figures: check no. of mechanisms to select the grid
    Ntyp=len(cyc_list)
    if (grid_fig == None):
        # auto-detection of size
        hspan=int(np.ceil(np.sqrt(Ntyp)))
        vspan=int(np.ceil(np.sqrt(Ntyp)))
    else:
        hspan,vspan=grid_fig

    fig,axes=plt.subplots(nrows=hspan,ncols=vspan,figsize=(hspan*plot_size,vspan*plot_size))
    ax=axes.flatten()
    # remove all axis marks
    [axii.axis('off') for axii in ax]
    try:
        posx=mech_container.MainGraph.graph['UndirPos']
    except:
        try:
            posx=mech_container.PosMainGraph
        except:
            posx=nx.spring_layout(mech_container.MainGraph,k=0.2,iterations=500)
    # Main plotting loop: generate the graph and then draw the typology over it
    for colx,axii,cycle in zip(array_colors_hex,ax,cyc_list):
        graph_skeleton_plot(mech_container.MainGraph,posx,axii,plt.cm.viridis)
        nx.draw_networkx_edges(mech_container.MainGraph,posx,ax=axii,
                                edgelist=cycle,width=5.0,edge_color=colx)
    plt.subplots_adjust(wspace=0, hspace=0)    
    plt.tight_layout()
    return fig,ax

def span_tree_generator(mech_obj,print_flag=True):
    '''For every mechanism in the list, proceed removing every edge 
    in the cycle (thus instantiating a tree) and then check whether this
    is isomorphic with any edge in the cycle. Similar procedure to mechanism_fetcher(),
    but more straightforward.
    Input: 
    - mech_obj. MechanismContainer() with a MechList with all mechanisms. The set of trees
    shall be complete if the set of mechanisms is also complete.
    - print_flag. Boolean, if True, print information about the progress of tree generation.
    Output:
    - None.
    - Fills the SpanTree attribute list of mech_obj with nx.Graph objects for every tree.'''
    span_trees=[]

    tree_mat_list=[]
    main_nodelist=mech_obj.MainGraph.nodes

    #function to check isomorphisms, using the node name as property
    #node_match_func=iso.categorical_node_match(attr="name",default="XXX")
    counter=0
    old_time=time.time()
    for im,mech in enumerate(mech_obj.MechList):
        start_node=mech.graph['StartNode']
        cycle_mech=nx.find_cycle(mech,start_node)
        n_edges = 0
        for edge_tuple in cycle_mech:
            tree_test=mech.copy()
            tree_test.remove_edges_from([edge_tuple])
            #check isomorphisms using adjacency matrices!
            
            #tree_matrix = np.array(nx.to_numpy_matrix(tree_test,nodelist=main_nodelist))
            tree_matrix = nx.to_numpy_array(tree_test,nodelist=main_nodelist)
            if (len(tree_mat_list) != 0):
                mc_tmp = tree_mat_list == tree_matrix
                mat_check = np.all(mc_tmp,(1,2))
                isomorph_flag = np.any(mat_check)
            else: 
                isomorph_flag = False

            if (isomorph_flag == False):
                tree_mat_list.append(tree_matrix)
                tree_test.graph['Type']='span_tree'
                span_trees.append(tree_test)
                n_edges += 1
            
            counter=counter+1
            if (counter % 100 == 0) and (print_flag == True):
                nw_time=time.time()
                lapse=(nw_time-old_time)
                print("%d edges tested, currently at mech. %d   (%.1f sec.)" % (counter,im+1,lapse))
                old_time=nw_time
    mech_obj.SpanTrees=span_trees
    return None 

def nodes_cycle_view(list_edges_cycle):
    '''Return the nodes (with duplicities) that participate in the
    cycle found by NetworkX.find_cycle()
    Input:
    - list_edges_cycle. List of 2-tuples with strings defining all edges
    in the mechanism, taken as output of nx.find_cycle()
    Output:
    - node_list. List of strings for node tags in the mechanism, duplicated 
    (end of an edge and beginning of the next).'''
    node_list=[]
    for edge_tuple in list_edges_cycle:
        node_list.extend([edge_tuple[0],edge_tuple[1]])
    return node_list
    
def pair_builder(node_string_list):
    '''Read a list of nodes and generate all consecutive edges
    Input:
    - node_string_list. List of strings for node tags.
    Output:
    - edge_pairs. List of tuples for all generated edges.'''
    edge_pairs=[]
    for ii in range(len(node_string_list)-1):
        pair=(node_string_list[ii],node_string_list[ii+1])
        edge_pairs.append(pair)
    return edge_pairs

def edge_species(edge_obj):
    '''Compare edge direction with the edgestring (reference of the input
    direction), so as to assign reactants and products accordingly (if the
    real traversal direction is oppossed to the edgestring, reactants and products
    in that edge will be inverted)
    Input:
    - edge_obj. EdgeView with data=True for a given edge.
    Output:
    - None
    - Fills the 'Rx' and 'Px' properties of the edges, corresponding to
    reactants and products in the true traversal direction.
    '''
    edgestr=edge_obj[2]['edgestring']
    graph_dir_str=edge_obj[0]+"_"+edge_obj[1]
    if (graph_dir_str == edgestr):
        edge_obj[2]["Rx"]=edge_obj[2]["reacs"]
        edge_obj[2]["Px"]=edge_obj[2]["prods"]
    else:
        edge_obj[2]["Rx"]=edge_obj[2]["prods"]
        edge_obj[2]["Px"]=edge_obj[2]["reacs"]
    return None

def graph_director(mech,get_plots=False):
    '''Convert a graph to a directed analogue, going from the starting point
    to all branches and in the requested direction of the cycle to form the product.
    To do this, the end nodes and the last cyclic node are found, building the sequences
    from each of these to the start point. Afterwards, these linear sequences give direction
    to a nx.DiGraph, inheriting all properties of the undirected nx.Graph
    Input:
    - mech. nx.Graph object for a given mechanism.
    - get_plots. Boolean, if True the directed mechanism is plotted throughout
    the function.
    Output:
    - mechD. nx.DiGraph with a fixed direction, inheriting all mech properties.'''
    #break the cycle and get the branching from the starting point of the cycle
    graph_seq=mech.copy()
    closer=graph_seq.graph['ClosingEdge']
    graph_seq.remove_edges_from([closer])
    startpt=graph_seq.graph['StartNode']
    follow_origin=nx.dfs_successors(graph_seq,startpt)
    from_origin=follow_origin[startpt]
    node_degrees=graph_seq.degree()
    end_nodes=[nodedeg[0] for nodedeg in node_degrees if nodedeg[1]==1]
    bpoints=[nodedeg[0] for nodedeg in node_degrees if (nodedeg[1]>2 and nodedeg[0]!=startpt)]
    #don't include the start node as an end!
    if (startpt in end_nodes):
        end_nodes.remove(startpt)
    #we will have as many branches as end nodes: try to find them going backwards
    origin_nodes=np.array([k for k,v in follow_origin.items()],dtype=object)
    destination_nodes=np.array([v for k,v in follow_origin.items()],dtype=object)
    ###prepare loop consideration
    loop_point_bool=np.array(closer) != startpt
    loop_point=(np.array(closer))[loop_point_bool][0]
    sequences=[]
    for end in end_nodes:
        ###build a sequence for each
        seqx=[end]
        boolflag=[end in a_trip for a_trip in destination_nodes]
        prev=origin_nodes[boolflag][0]
        seqx.append(prev)
        end_seq=(prev == startpt)
        while (end_seq == False):
            boolflag=[prev in a_trip for a_trip in destination_nodes]
            prev=origin_nodes[boolflag][0]
            seqx.append(prev)
            end_seq=(prev == startpt)
        seqx=seqx[::-1] #so they finish with the end nodes
        #handle the cyclic sequence
        seq_end=seqx[-1]
        if (seq_end == loop_point):
            seqx.append(startpt)
            mech.graph['CyclicNodes']=np.array(seqx)
        sequences.append(seqx)
    sequences=np.array(sequences,dtype=object)    
    #create the directed graph
    mechD=nx.DiGraph()
    mechD.add_nodes_from(mech.nodes.data())
    for seqx in sequences:
        for ii in range(len(seqx)-1):
            n0=seqx[ii]
            n1=seqx[ii+1]
            pair=(n0,n1)
            attr_or=mech[n0][n1]
            mechD.add_edges_from([(n0,n1,attr_or)])
    #Now we can assign proper reactants and products to each edge
    for edge in mechD.edges(data=True):
        edge_species(edge)        
    mechD.graph=mech.graph
    #assign undirected layout as property, for better plotting
    pos=nx.spring_layout(mech)
    mechD.graph['UndirPos']=pos
    if (get_plots == True):
        plt.figure()
        nx.draw(mechD,pos,with_labels=True)
    mechD.graph['Sequences']=sequences
    return mechD
    
def timeline(mechD):
    '''Get full list of species entering/exiting for nodes
    and edges from a DIRECTED mechanism, then transform back
    to undirected one once relationships have been properly treated.
    Input:
    - mechD. nx.DiGraph() traversed from start point to all branch ends and in
    the forward direction of the mechanism
    Output:
    - mechU. nx.Graph(), undirected, containing traversal information from the DiGraph
    as a set of node/edge properties -> 'EdgesAfter', 'EdgesBefore', 'ReacsAfter',
    'ProdsBefore'. Called a *traversed mechanism*'''
    #remove the closing edge from mech
    startpt=mechD.graph['StartNode']
    m_tree=mechD.copy()
    m_tree.remove_edge(*mechD.graph['ClosingEdge'])
    #build SUCCESSOR dictionaries for every node, in both dir. and rev. directions
    #and generate all possible edges
    main_dict=nx.dfs_successors(m_tree,startpt)
    #fetch the sequences sed to generate the directed graph
    sequences=mechD.graph['Sequences']
    #Node main loop
    for node in mechD.nodes(data=True):
        #Check if the node is in-cycle
        node_cyclic=node[0] in mechD.graph['CyclicNodes']
        ### Process what's AFTER
        after_node=nx.dfs_successors(m_tree,node[0])
        edges_after=[]
        nodes_after=[]
        for keyx in after_node.keys():
            destinations=after_node[keyx]
            edgelist=[(keyx,valx) for valx in destinations]
            if keyx not in nodes_after:
                nodes_after.append(keyx)
            new_nodes=[valx for valx in destinations if valx not in nodes_after]
            nodes_after.extend(new_nodes)
            edges_after.extend(edgelist)
        #finally, we should add the closing edge, which is after all in-cycle nodes
        if (node_cyclic == True):
            edges_after.append(mechD.graph['ClosingEdge'])
        node[1]['EdgesAfter']=edges_after
        ### Process what's BEFORE. We need to consider branching points
        #for all non-cyclic nodes
        edges_before=[]
        if (node_cyclic == False):
            which_seq=[node[0] in seqx for seqx in sequences]
            sel_seq=np.array(sequences)[which_seq][0]
            #the branching point will be the last cycle match
            seq_cyc=[nx in mechD.graph['CyclicNodes'] for nx in sel_seq]
            bp=np.array(sel_seq)[seq_cyc][-1]
            ref_node=bp
        else:
            #nodes in the cycle are their own ref
            ref_node=node[0]
        ###analyze whether the node is in the sequences
        for seqx in sequences:
            node_seq=(ref_node in seqx)
            if (node_seq == False):
                #consider ALL nodes to be before the match
                gen_edges=pair_builder(seqx)
            else:
                #find the match, and trim until it happens
                seq_arr=np.array(seqx)
                ndx=np.argwhere(seq_arr == ref_node)[0][0]
                trim_seq=seq_arr[:ndx+1]
                gen_edges=pair_builder(trim_seq)
                #for branched cases, consider all the branch where they are
                if (node_cyclic == False) and (node[0] in seqx):
                    ndx2=np.argwhere(seq_arr == node[0])[0][0]
                    rest_seq=seq_arr[ndx:ndx2+1] #from bp to node
                    rest_edges=pair_builder(rest_seq)
                    gen_edges=gen_edges+rest_edges
                    
            nw_edges=[edge for edge in gen_edges if edge not in edges_before]
            edges_before.extend(nw_edges)
        node[1]['EdgesBefore']=edges_before
        #get reactants and products
        raw_rx_list=[mechD.edges[after]['Rx'] for after in node[1]['EdgesAfter']]
        rx_list=[]
        for item in raw_rx_list:
            rx_list.extend(item)
        node[1]['ReacsAfter']=rx_list
        raw_px_list=[mechD.edges[before]['Px'] for before in node[1]['EdgesBefore']]
        px_list=[]
        for item in raw_px_list:
            px_list.extend(item)
        node[1]['ProdsBefore']=px_list

    ###And we must also process the information for edges
    ###Use the first node as ref. for edges before and the second as
    ###ref. for the edges that are after, with an exception for the closing edge
    for edge in mechD.edges(data=True):
        n0=edge[0]
        n1=edge[1]
        ###exception: for the closing edge, second node (n1) can be start point. Avoid it.
        if (n1 == startpt):
            #we will consider that the end is also n0
            n1=n0
        edge[2]['EdgesBefore']=mechD.nodes[n0]['EdgesBefore']
        edge[2]['EdgesAfter']=mechD.nodes[n1]['EdgesAfter']
        edge[2]['ProdsBefore']=mechD.nodes[n0]['ProdsBefore']
        edge[2]['ReacsAfter']=mechD.nodes[n1]['ReacsAfter']
        
    #Transform back to undirected, as all information from traversal is already
    #available in the generated properties
    mechU=mechD.to_undirected()
    return mechU

def span_tree_closer(mechs,print_flag=True):
    '''For a MechanismContainer with a set of spanning trees in the SpanTree list,
    analyze the first edge that can close every tree to a valid    mechanism, and associate them.
    Input:
    - mechs. MechanismContainer object with a list of *traversed* mechanisms and
    with generated SpanTrees.
    - print_flag. Boolean, if True, print information about the progress of tree closure.
    Output:
    - None.
    - For every tree, adds the nx.Graph object for the matching mechanism ('MatchedMech') and
    the correspnding diff. edge ('FirstMechEdge') '''
    strees=mechs.SpanTrees
    
    #use adjacency matrices (only upper diagonal)
    main_nodelist = mechs.MainGraph.nodes
    the_nodelist = list(main_nodelist)
    tree_matrices = [np.triu(nx.to_numpy_matrix(st,nodelist=main_nodelist)) for st in strees]
    mech_matrices = [np.triu(nx.to_numpy_matrix(mech,nodelist=main_nodelist)) for mech in mechs.MechList]
    for it,st in enumerate(tree_matrices):
        #Find mechanisms only differing from the tree in one edge
        #How: nx.difference() between MECH and TREE provides edges that are in MECH but not in TREE

        diff_mat = [(st == mech_mat) for mech_mat in mech_matrices]
        chosen_diffs = [(ii,tuple(np.argwhere(dx == False)[0])) for (ii,dx) in enumerate(diff_mat) if np.sum(~dx) == 1]
        
        #difference() does not copy the data
        #choose the edge which has more elements AFTER as the first edge that can close the tree
        #-> feasible implementation of Kozuch's criterion
        Nbest=0
        for item in chosen_diffs:
            diff_indices = item[1]
            diffedge = tuple(the_nodelist[jj] for jj in diff_indices)
            #diffedge = tuple(str(x) for x in ch[1])
            mech_query=mechs.MechList[item[0]]
            try:
                edge_query=mech_query[diffedge[0]][diffedge[1]]['EdgesAfter']
                Nafter=len(edge_query)
            except:
                #no EdgesAfter properties implies that the edge is at the end
                Nafter=0
                
            if (Nafter >= Nbest):
                Nbest=Nafter
                matched_mech=mech_query
                match_ndx=item[0]
                match_edge=diffedge
        #assign to tree!
        tree_graph = strees[it]
        tree_graph.graph['MatchedMech']=matched_mech
        tree_graph.graph['FirstMechEdge']=diffedge
        if (it % 200 == 0) & (print_flag == True):
            print("Closing tree no. %d" % it)
    return None

def graph_skeleton_plot(graphD,pos,ax_draw,cmap_obj):
    '''
    Basic graph plotting without initialization of a new figure, for
    embedding in subplot structures.
    Input:
    - graphD. Instantiated nx.Graph object
    - pos. Valid graph layout for graphD.
    - ax_draw. Axes object in which to draw the graph
    - cmap_obj: Matplotlib colormap
    Output:
    - None. Draws the image.
    '''
    energy_dict=nx.get_node_attributes(graphD,'energy')
    e_map=[item[1] for item in energy_dict.items()]
    nx.draw(graphD,pos,ax=ax_draw,with_labels=False,node_size=100,
                node_color=e_map,cmap=cmap_obj,alpha=1.0)
    return None


def plot_directed_network(graphD,e_string='energy',parent_layout=True,no_labels=False,ax=None,
    figsize=(6,6),node_names=True,current_cmap=None,alpha_val=1.0,pos=None):
    '''Improved plot generation for either nx.DiGraph or nx.Graph objects, including the energies of nodes and 
    edges, and coloring nodes by relative energies: ask for either 'energy' or 'EnergyCorr'
    Input:
    - graphD. nx.DiGraph or nx.Graph object for a network, mechanism or tree.
    - e_string. String, requested property to plot: 'energy' or 'EnergyCorr'
    - parent_layout. Boolean, if True, use the layout of the original network (saved as 'MainLayout'
    in the mechanism) for plotting. Else, consider the position of the undirected graph ('UndirPos').,
    Avoid using nx.spring_layout() on the nx.DiGraph: positions are weird.
    - no_labels: Boolean, if True, don't plot energy labels.
    - ax. Matplotlib axes object. If None, new Figure and Axes will be instantiated.
    - figsize: Tuple of ints, size of the generated figure
    - node_names: Boolean, if True, include node names.
    - current_cmap. Matplotlib colormap object. If None, use viridis.
    - alpha_val. Float, transparency for nodes.
    Output:
    - fig, ax. Matplotlib figure.'''
    fig=None
    if (not ax):
        fig=plt.figure(figsize=figsize)
        ax=fig.add_subplot(1,1,1)
    if (not current_cmap):
        current_cmap = plt.cm.viridis
    #define the graph layout
    if (parent_layout == True and graphD.graph.get('MainLayout')):
        pos=graphD.graph['MainLayout']
    elif pos == None:
        #By default consider the UndirPos inherited for directed mechanisms (as nx.spring_layout() does not
        # work too well for directed graphs.). If it is not assigned (e.g for non-processed undirected
        # graphs), build a new graph layout.
        try:
            pos=graphD.graph['UndirPos']
        except:
            #harsher k and iteration variabls allow for better-looking plots
            pos=nx.spring_layout(graphD,k=0.5,iterations=150)

    #obtain dict of energies: for colormapping and for labels
    energy_dict=nx.get_node_attributes(graphD,e_string)
    #unpack values & draw
    e_map=[item[1] for item in energy_dict.items()]
    nx.draw(graphD,pos,with_labels=node_names,node_size=300,node_color=e_map,cmap=current_cmap,alpha=alpha_val,
        font_weight='bold')
    #new dictionary for new positions (same x, slight y-shift)
    pos_attrs={}
    for node, coords in pos.items():
        pos_attrs[node] = (coords[0], coords[1] - 0.075)
    
    #plot only LABELS: we should convert floats to strings with a given no. of digits
    #Nodes
    label_e_dict={}
    for k,v in energy_dict.items():
        v_form="%6.1f" % v
        label_e_dict[k]=v_form
    if (no_labels == False):
        nx.draw_networkx_labels(graphD, pos_attrs, labels=label_e_dict)
    #Edges
    edge_energy_dict=nx.get_edge_attributes(graphD,e_string)
    label_edge_dict={}
    for k,v in edge_energy_dict.items():
        v_form="%6.1f" % v
        label_edge_dict[k]=v_form
    if (no_labels == False):
        nx.draw_networkx_edge_labels(graphD,pos,edge_labels=label_edge_dict,font_color="red")
    plt.tight_layout()
    return fig,ax

def plot_interactive(graphD,save_fig=False,file_img=None,e_string='energy',parent_layout=True,no_labels=False,
    figsize=(6,6),node_names=True,current_cmap=plt.cm.viridis,alpha_val=1.0):
    '''Improved plot generation for either nx.DiGraph or nx.Graph objects, including the energies of nodes and 
    edges, and coloring nodes by relative energies: ask for either 'energy' or 'EnergyCorr'
    Input:
    - graphD. nx.DiGraph or nx.Graph object for a network, mechanism or tree.
    - save_fig. Boolean, if True save the figure to file.
    - file_img. String, file to save the image to.
    - e_string. String, requested property to plot: 'energy' or 'EnergyCorr'
    - parent_layout. Boolean, if True, use the layout of the original network (saved as 'MainLayout'
    in the mechanism) for plotting. Else, consider the position of the undirected graph ('UndirPos').,
    Avoid using nx.spring_layout() on the nx.DiGraph: positions are weird.
    - no_labels: Boolean, if True, don't plot energy labels
    - figsize: Tuple of ints, size of the generated figure
    - node_names: Boolean, if True, include node names.
    - current_cmap. Matplotlib colormap object.
    - alpha_val. Float, transparency for nodes.
    Output:
    - fig, ax. Matplotlib figure.'''
    fig=plt.figure(figsize=figsize)
    ax=fig.add_subplot(1,1,1)
    #define the graph layout
    if (parent_layout == True):
        pos=graphD.graph['MainLayout']
    else:
        #By default consider the UndirPos inherited for directed mechanisms (as nx.spring_layout() does not
        # work too well for directed graphs.). If it is not assigned (e.g for non-processed undirected
        # graphs), build a new graph layout.
        try:
            pos=graphD.graph['UndirPos']
        except:
            #harsher k and iteration variabls allow for better-looking plots
            pos=nx.spring_layout(graphD,k=0.5,iterations=150)

    #obtain dict of energies: for colormapping and for labels
    energy_dict=nx.get_node_attributes(graphD,e_string)
    #unpack values & draw
    e_map=[item[1] for item in energy_dict.items()]
    nx.draw(graphD,pos,with_labels=False,node_size=300,node_color=e_map,cmap=current_cmap,alpha=alpha_val)

    #use plt.annotate for draggable labels
    if (node_names == True):
        #iterate through position dictionary
        for node in pos.keys():
            pos_value = pos[node]
            plt.annotate(node,pos_value,fontweight='bold',fontsize=14,
            horizontalalignment='center',verticalalignment='center').draggable()

    #new dictionary for new positions (same x, slight y-shift)
    pos_attrs={}
    for node, coords in pos.items():
        pos_attrs[node] = (coords[0], coords[1] - 0.075)

    #use nodename keys to access positions and energies
    if (no_labels == False):
        #nx.draw_networkx_labels(graphD, pos_attrs, labels=label_e_dict)
        for node in pos_attrs.keys():
            pos_value = pos_attrs[node]
            e_value = energy_dict[node]
            e_value_format = "%6.1f" % e_value
            plt.annotate(e_value_format,pos_value,verticalalignment='bottom',fontsize=12).draggable()
    #Edges: use default implementation to have edge names in the middle
    edge_energy_dict=nx.get_edge_attributes(graphD,e_string)
    label_edge_dict={}
    for k,v in edge_energy_dict.items():
        v_form="%6.1f" % v
        label_edge_dict[k]=v_form
    if (no_labels == False):
        nx.draw_networkx_edge_labels(graphD,pos,edge_labels=label_edge_dict,font_color="red")
    #Image generation
    if (save_fig == True):
        plt.tight_layout()
        plt.savefig(file_img,dpi=300)
    return fig,ax

def post_director(mechU):
    '''Take the sequences stored in a fully computed undirected mechanism
    and set directions: regenerate the directed graph, if desired, but with
    all final properties of the undirected one used in TOF computation.
    Input:
    - mechU: nx.Graph, undirected.
    Output:
    - mechD. nx.DiGraph, following stored 'Sequences' property in the
    mechU nx.Graph and inheriting all its properties.'''
    mechD=nx.DiGraph()
    #Handle node attributes
    mechD.add_nodes_from(mechU.nodes.data())
    sequences=mechU.graph['Sequences']
    for seqx in sequences:
        for ii in range(len(seqx)-1):
            n0=seqx[ii]
            n1=seqx[ii+1]
            pair=(n0,n1)
            #Handle edge attributes
            attr_or=mechU[n0][n1]
            mechD.add_edges_from([(n0,n1,attr_or)])
    #Handle graph attributes
    mechD.graph=mechU.graph    
    return mechD
            
def binary_savefile(mech_container,outfile="binary_network.bin"):
    '''Save a MechContainer object as a Pickle. 
    The goal is to store pre-computed containers (traversed & with corresponding
    spanning trees) for further processing, specially for very large networks.
    Input:
    - mech_container. MechContainer object to be saved
    - outfile. String, name of the file to store our container
    Output:
    - None. 
    - Generates file.'''
    binary_file=open(outfile,'ab')
    pickle.dump(mech_container,binary_file)
    binary_file.close()    
    return None
    
def binary_loadfile(infile="binary_network.bin"):
    '''Load a MechContainer object from a Pickle. 
    The goal is to get a stored pre-computed container (traversed & with corresponding
    spanning trees) for further processing, specially for very large networks.
    Input:
    - infile. String, name of the file to load our container from
    Output:
    - mech_read. MechContainer object from Pickle'''
    binary_file=open(infile,'rb')
    mech_read=pickle.load(binary_file)
    binary_file.close()    
    return mech_read

def generate_graphml_network(nodefile,edgefile,closing_edges,start_node, graph_fname="reaction_network.xml"):
    '''Instantiate a graph object from input files (via graph_instantiator() function)
    and save it into GraphML format (XML specification for graphs).
    Input:
    - nodefile. String, file to take node data from.
    - edgefile. String, file to take edge data from.
    - closing_edges. List of strings referring to the edges that
    can close a catalytic cycle in the main network.
    - start_node. String, name of the starting node.
    - graph_fname. String, name of the saved XML file.
    Output:
    - None. 
    - Generates XML file with the basic information saved in XML format
    '''
    G=graph_instantiator(nodefile,edgefile,closing_edges)
    #Process graph properties: add StartNode, and transform ClosingEdgesList to a string
    #so it can be handled by NetworkX GraphML engine
    G.graph['ClosingEdgesList']="====".join(G.graph['ClosingEdgesList'])
    nx.write_graphml(G,graph_fname)
    return None

def unfold_profiles(graphNX,used_fig=None,used_axis=None,e_string='energy',put_labels=True,xw=2.0):
    '''
    For a single, pre-processed mechanism, unfold the reaction network to build an energy profile.
    Input:
    - graphNX. Graph object for the mechanism, must have been traversed so as to have the Sequences property.
    - used_fig. plt.figure() instance. If None, instantiate a new one.
    - used_axis. Matplotlib.axes() object. If None, instantiate a new one.
    - e_string. String, energy to be fetched from the graph: can be either 'energy' or 'EnergyCorr'
    - put_labels. Boolean. If True, include labels for nodes along the profile.
    - xw. Float, length of lines along x-axis.
    Output:
    - fig, ax. plt.figure() and plt.axes() instances, only if they were not defined upon call.
    - None, elsewhere. Generates a plot.
    '''
    # Get sequences of the mechanism to build the profile
    seqx = graphNX.graph['Sequences']
    used_labels = []

    # Fig/ax object handling
    if (used_fig == None) or (used_axis == None):
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
    else:
        fig = used_fig
        ax = used_axis
    
    # Loop through the list of sequences
    for s in seqx:
        npts = 2*len(s) - 1
        seq_info = []
        # Iteration along every sequence
        for ii,item in enumerate(s):
            e_node = graphNX.nodes[item][e_string]
            # Entries as tuples of the form (LABEL,ENERGY,STAGE)
            node_stage = 2*(ii+1) - 1
            node_entry = (item,e_node,node_stage)
            seq_info.append(node_entry)
            # To define edges, find next element except if current is the last
            if (ii != len(s) - 1):
                next_item = s[ii+1]
                e_edge = graphNX.edges[item,next_item][e_string]
                edge_stage = 2*(ii+1)
                edgestring = item+"_"+next_item
                edge_entry = (edgestring,e_edge,edge_stage)
                seq_info.append(edge_entry)
        # Once the mechanism is processed into a list of tuples, get a valid XY array to plot it
        # Map X coordinate to integers related to the stage, Y is the energy
        Nentries = 2*npts # every entry has TWO x points to form a line segment
        xy_array = np.zeros((Nentries,2))
        for entry in seq_info:
            # define integers for array indexing and for X-coord definition
            # consider 2N-1 to 2N, where N is STAGE - 1 (to match Py indexing)
            x1 = 2*(entry[2] - 1) 
            x2 = 2*(entry[2] - 1) + 1
            y = entry[1]
            xy_array[x1,0] = x1*xw
            xy_array[x2,0] = x2*xw
            xy_array[x1,1] = y
            xy_array[x2,1] = y
            # plot a line for the current state
            ax.plot((x1*xw,x2*xw),(y,y),linewidth=3,color='black')
            # put draggable annotations only for NODES (edge names are predictable edgestrings)
            if (put_labels == True):
                current_label = entry[0]
                if (current_label not in used_labels) and ("_" not in current_label):
                    plt.annotate(current_label,(x1*xw,y),verticalalignment='bottom').draggable()
                    used_labels.append(current_label)

        #plot the skeleton (connecting lines)
        ax.plot(xy_array[:,0],xy_array[:,1],'--')
        ax.set_ylabel("energy/kcal mol$^{-1}$")
        ax.set_xticks([])

    # return fig and ax if they had not been defined
    
    if (used_fig == None) or (used_axis == None):
        return fig,ax
    else:
        return None
     
# Interface with ioChem-BD reaction networks, stored in DOT format
# Read as MultiGraph, then process to get valid graphs with same attributes
# and structure as graphs read from text input from graph_instantiator()

# Helper functions
def fill_missing_energy(G,node1,node2,e_string='energy'):
    '''
    Substitute the energy of a given edge by the energy of the
    largest-energy node involved in the edge.
    Input:
    - G. nx.Graph object.
    - node1, node2. Strings. Names of the nodes involved in the edge.
    Output:
    - None. Changes the graph in-place.
    '''
    e1 = G.nodes[node1]['energy']
    e2 = G.nodes[node2]['energy']
    e_TS = max(e1,e2)
    G.edges[node1,node2]['energy'] = e_TS
    return None


# Multi-graph based implementation
# The workflow: read as MultiGraph, convert to list-based Graph, then find unique network

def dot_reader(graph_filename):
    '''
    Read a DOT file as a NetworkX.MultiGraph object and parse the comma-separated
    lists of attributes (energies, keys...) from ioChem into valid Python lists.
    Input:
    - graph_filename. String, name of the DOT file to be read
    Output:
    - Gworking. nx.MultiGraph object with attribute lists for keys and energies.   
    '''
    Gmulti = read_dot(graph_filename)
    for nd in Gmulti.nodes(data=True):
        # for key and energy, remove leading/trailing quotes and split by comma
        # then remove original entries
        key_list = (nd[1]['key'][1:-1]).split(",")
        nd[1]['key'] = [int(keyval) for keyval in key_list]
        e_list = (nd[1]['energy'][1:-1]).split(",")
        nd[1]['energy'] = [float(evalue) for evalue in e_list]

    # copy the nodes to a new graph, then add edges
    Gworking = nx.Graph()
    Gworking.add_nodes_from(Gmulti.nodes(data=True))

    # we need the keys= argument because we are working with a MultiGraph
    for edge in Gmulti.edges(data=True,keys=True):
        # convert key and energy to valid numbers: key is not in dict, but as multigraph indx
        edge[3]['key'] = int(edge[2][1:-1])
        edge[3]['energy'] = float(edge[3]['energy'][1:-1])
        # check whether the edge is already there
        edge_known = Gworking.has_edge(edge[0],edge[1])
        if (edge_known == False):
            # direct addition: just transform key and energy to lists
            Gworking.add_edge(edge[0],edge[1])
            attr_dict = edge[3]
            attr_dict['key'] = [attr_dict['key']]
            attr_dict['energy'] = [attr_dict['energy']]
            Gworking.edges[edge[0],edge[1]].update(attr_dict)
        else:
            # if the edge was already present, append energy and key
            ed_dict = Gworking.edges[edge[0],edge[1]]
            ed_dict['key'].append(edge[3]['key'])
            ed_dict['energy'].append(edge[3]['energy'])
    return Gworking

def dot_processor(G_input,used_keys='all'):
    '''
    Transform a parsed MultiGraph from dot_reader() into a valid 
    unique nx.Graph object that can be directly passed to gTOFfee, 
    adding required properties.
    # MultiGraph adaptation workflow.
        1. Process energy lists, fetching only requested keys
        2. Average energies to get a single value per node/edge
    # gTOFfee adaptation workflow
        1. Add "name" to the node properties
        2. Fill energies of missing TSs 
        3. Identify closing edges, save them and set energies
        4. Save edgestrings
        5. Save 'reacs' and 'prods'. Currently we have empty placeholders, should add
        formula parsing in the future.
    Input:
    - G_input. Parsed nx.MultiGraph() from DOT file preprocessing.
    - used_keys. List of integer IDs for the key values to include in the graph. 
    Default is 'all', considering all existing keys
    Output:
    - G_uniq. nx.Graph, filled with all required values.
    '''
    # Some initializations
    closer_list = []

    # Key selection
    if (used_keys == 'all') or (type(used_keys) != list):
        # get the list of all possible key values looping through nodes
        all_key_vals = [item for entry in G_input.nodes(data='key') for item in entry[1]]
        used_keys = list(set(all_key_vals))
    
    # Instantiate a new Graph object
    G_uniq = nx.Graph()
    
    # Process all nodes in the input
    for nd in G_input.nodes(data=True):
        attr_dict = nd[1]
        keys = attr_dict['key']
        energvals = attr_dict['energy']
        used_energies = [energ for k,energ in zip(keys,energvals) if k in used_keys]
        # Only add entries with at least one valid key
        if (len(used_energies) > 0):
            G_uniq.add_node(nd[0])
            # Add properties with .update(), average the energies if repeated
            # For any well-designed, valid network, energy values for common nodes must be the same.
            G_uniq.nodes[nd[0]].update(nd[1])
            avg_val = np.mean(used_energies)
            variation = np.max(used_energies) - np.min(used_energies)
            # Use the average energy in the new graph, also remove the key (not required anymore)
            del G_uniq.nodes[nd[0]]['key']
            G_uniq.nodes[nd[0]]['energy'] = avg_val
            # Check for unconsistent values: variation is larger than 5% of result. Flag these situations!
            unconsistency_flag = (variation > np.abs(0.05*avg_val))
            if (unconsistency_flag): 
                print(used_energies,variation,0.05*avg_val)
                print("Node %s has mismatched energy values in input: %6.2f variation from used avg. value" % (str(nd[0]),variation/avg_val*100))
                print("Please check the input carefully")
            # Add name property: these MUST be unique for some functions to work properly
            # to assure, assign 'name' as the unique node ID and generate a tag from
            # from label, removing all after newline and the leading quote to allow renaming too
            
            G_uniq.nodes[nd[0]]['name'] = str(nd[0])
            G_uniq.nodes[nd[0]]['nametag'] = attr_dict['label'].split("\\n")[0][1:]
            
    # Process edges
    for ed in G_input.edges(data=True):
        attr_dict = ed[2]
        keys = attr_dict['key']
        energvals = attr_dict['energy']
        used_energies = [energ for k,energ in zip(keys,energvals) if k in used_keys]
        # Only add entries with at least one valid key
        if (len(used_energies) > 0):
            G_uniq.add_edge(ed[0],ed[1])
            # Add properties via .update(), average the energies if repeated
            G_uniq.edges[ed[0],ed[1]].update(ed[2])
            avg_val = np.mean(used_energies)
            variation = np.max(used_energies) - np.min(used_energies)
            # use the average energy in the new graph, also remove the key (not required anymore)
            del G_uniq.edges[ed[0],ed[1]]['key']
            G_uniq.edges[ed[0],ed[1]]['energy'] = avg_val

            # Add edgestring, reacs and prods
            edgestring = ed[0] + "_" + ed[1]
            G_uniq.edges[ed[0],ed[1]]['edgestring'] = edgestring
            # By now, only placeholders for reacs/prods are supported
            G_uniq.edges[ed[0],ed[1]]['reacs'] = ['x']
            G_uniq.edges[ed[0],ed[1]]['prods'] = ['x']

            # Handle missing and closing edges, which are labeled in the corresponding entry of the MultiGraph
            label_string = ed[2]['label']
            missing_flag = 'missing' in label_string
            closing_flag = 'Closing' in label_string
            if (missing_flag and ~closing_flag):
                # For missing edges, assign the energy of the highest connected node
                fill_missing_energy(G_uniq,ed[0],ed[1])
            elif (closing_flag):
                # Store the closing edges in a list for automatic handling
                # Set last node's energy (to the energy of the last node!) and correct edgestring
                e_last = G_uniq.nodes[ed[1]]['energy']
                G_uniq.edges[ed[0],ed[1]]['energy'] = e_last
                closer_str = ed[1]+"_"+ed[0]
                G_uniq.edges[ed[0],ed[1]]['edgestring'] = closer_str
                closer_list.append(closer_str)
        # save the list as graph property
        G_uniq.graph['ClosingEdgesList'] = closer_list
    return G_uniq

def node_renamer(in_graph):
    '''For every node and edge in the graph, replace the ID by the 'nametag' field.
    Don't use if names are not valid identifiers (e.g. repeated names).
    Input:
    - in_graph. nx.Graph object to be modified.
    Output:
    - out_graph. nx.Graph object with replaced names.'''
    # Generate a dictionary mapping IDs with nametag
    dict_nodes = {nd[0]:nd[1]['nametag'] for nd in in_graph.nodes(data=True)}
    out_graph = nx.relabel_nodes(in_graph,mapping=dict_nodes,copy=True)
    # also change the closing edge(s)!
    closures = [closer.split('_') for closer in out_graph.graph['ClosingEdgesList']]
    new_closures = [dict_nodes[cl[0]] + "_" + dict_nodes[cl[1]] for cl in closures]
    out_graph.graph['ClosingEdgesList'] = new_closures
    
    # check that no information has been lost: if new names are not unique IDs,
    # some nodes may have been overwritten
    Nnodes_in = len(in_graph.nodes)
    Nnodes_out = len(out_graph.nodes)
    valid_id_flag = (Nnodes_in == Nnodes_out)
    if (not valid_id_flag):
        print("The 'name' field did not contain valid, unique identifiers")
        print("Original IDs are kept")
        out_graph = in_graph.copy()
    return out_graph

def read_iochem_graph(graph_filename,used_keys='all',rename_nodes=False):
    '''Wrapper function: read an ioChem-downloaded DOT file to a valid nx.Graph
    object that can be passed to gTOFfee. 
    Key aspects:
    # Missing edge energies will be assigned as the largest energy of the connected nodes.
    # Reactants and products are NOT currently detected: we only add placeholders. In the future,
    some formula parsing should allow to include this information automatically.
    Input:
    - graph_filename. String, name of the DOT file to be read (downloaded from ioChem-BD).
    - used_keys. List of integers: IDs for the series that should be included in the graph.
    Default value, 'all', automatically fetches all existing keys
    - rename_nodes. Boolean. If True, update node names in nodes and edges by the 'name' property,
    taken from labels.
    Output:
    - G_out. nx.Graph, gTOFfee-compatible. Same fields & properties as if read from plain text files.
    '''
    G_raw = dot_reader(graph_filename)
    G_out = dot_processor(G_raw,used_keys)
    if (rename_nodes):
        G_out = node_renamer(G_out)
    return G_out
