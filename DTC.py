
import nx_reaxgraf as rg
import nx_gTOFfee as gtof
import numpy as np


def print_Xtof(mechs_dtc,n_cycles,all_n_n,all_e_n,typology_list) -> None:
    '''Print degrees of turnover frequency control (Xtof) to STDOUT.
    - mechs_dtc. List of dictionaries of Xtof values of each intermediate and transition state for
    each mechanism
    - n_cycles. Total number of distinct cycles. Used purely for indexing
    - all_n_n. List with names of all nodes (whatever was used as keys in 'mechs_dtc')
    - all_e_n. List with names of all edges (whatever was used as keys in 'mechs_dtc')
    - typology_list. Iterable with typology indices for all mechanisms
    Output:
    - prints to STDOUT.
    '''
    fcw = 24 # First column width
    cw = 12 # Column width
    
    print("\n","-"*80,"\n  Degrees of TOF control\n","-"*80)
    
    for c_idx in range(n_cycles): # Group results by cycle
        c_title = "\nCycle %2i:" % (c_idx+1)
        c_title = c_title.ljust(fcw+5)
        c_b = True      
        
        prnt = "\n     Intermediates"

        for n_n in all_n_n:
            prnt += "\n" + n_n.ljust(fcw)

            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    prnt += "%*.3g" % (cw, m_dict[n_n])
                    if c_b: c_title += f" mech. {m_idx}".ljust(cw) # Creates title row

            c_b = False

        prnt += "\n     Transition states"

        for e_n in all_e_n:
            prnt += "\n" + e_n.replace("_"," ").ljust(fcw)

            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    try:
                        prnt += "%*.3g" % (cw, m_dict[e_n])
                    except KeyError:
                        prnt += "N/A".rjust(cw)

        print(c_title,prnt)

def print_avg_Xtof(mechs_dtc,n_cycles,all_n_n,all_e_n,typology_list,tof_per_mech) -> None:
    '''Print degrees of turnover frequency control (Xtof) to STDOUT.
    Xtof presented as weighted averages between all mechanisms belonging to a cycle. Also returns
    Xtof values from the mechanism with the highest TOF
    - mechs_dtc. List of dictionaries of Xtof values of each intermediate and transition state for
    each mechanism
    - n_cycles. Total number of distinct cycles. Used purely for indexing
    - all_n_n. List with names of all nodes (whatever was used as keys in 'mechs_dtc')
    - all_e_n. List with names of all edges (whatever was used as keys in 'mechs_dtc')
    - typology_list. Iterable with typology indices for all mechanisms
    - tof_per_mech. Iterable with TOFs calculated for all mechanisms
    Output:
    - prints to STDOUT.
    '''
    fcw = 24 # First column width
    cw = 12 # Column width
    
    print("\n","-"*80,"\n  Degrees of TOF control\n","-"*80)
    
    for c_idx in range(n_cycles): # Group results by cycle
        c_title = "\nCycle %2i:" % (c_idx+1)
        c_title = c_title.ljust(fcw+5)
        c_b = True

        tofs_in_cycle = []
        for i, tof in enumerate(tof_per_mech):
            if typology_list[i] == c_idx:
                tofs_in_cycle.append(tof)
        tofs_in_cycle = np.array(tofs_in_cycle)
        tof_sum = np.sum(tofs_in_cycle)
        weights = tofs_in_cycle / tof_sum
        max_tof = max(tofs_in_cycle)
        
        prnt = "\n     Intermediates"

        for n_n in all_n_n:
            prnt += "\n" + n_n.ljust(fcw)
            if c_b: c_title += "Wtd. avg.   Max TOF"
            Xtofs = []
            X_max_tof = 5.
            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    Xtofs.append(m_dict[n_n])
                    if tof_per_mech[m_idx] == max_tof: 
                        X_max_tof = m_dict[n_n]
                        if c_b: c_title += f" (mech. {m_idx})"
            Xtofs = np.array(Xtofs)
            weighted_Xtofs = weights * Xtofs
            Xtof_wavg = np.sum(weighted_Xtofs)
            prnt += "%*.3g" % (cw, Xtof_wavg)
            if X_max_tof != 5.: prnt += "%*.3g" % (cw, X_max_tof)
            c_b = False

        prnt += "\n     Transition states"

        for e_n in all_e_n:
            prnt += "\n" + e_n.replace("_"," ").ljust(fcw)

            Xtofs = []
            X_max_tof = 5.
            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    try:
                        Xtofs.append(m_dict[e_n])
                        if tof_per_mech[m_idx] == max_tof:
                            X_max_tof = m_dict[e_n]
                    except KeyError:
                        # Xtofs for transition states that are not included in the 
                        # mechanism are set to 0. Note that all mechanisms are still
                        # included in calculating the weights
                        Xtofs.append(0)
            Xtofs = np.array(Xtofs)
            weighted_Xtofs = weights * Xtofs
            Xtof_wavg = np.sum(weighted_Xtofs)
            prnt += "%*.3g" % (cw, Xtof_wavg)
            if X_max_tof != 5.: prnt += "%*.3g" % (cw, X_max_tof)

        print(c_title,prnt)

def print_reduced_Xtof(mechs_dtc,n_cycles,all_n_n,all_e_n,typology_list,tof_per_mech,
    hide_lessthan=0.001) -> None:
    '''
    Print degrees of turnover frequency control (Xtof) to STDOUT.
    Reduced output Only shows weighted averages of Xtofs of each mechanism for each cycle. Anything
    very small is shown as '-' to reduce visual clutter. Rows with no significant Xtofs are hidden.
    - mechs_dtc. List of dictionaries of Xtof values of each intermediate and transition state for
    each mechanism
    - n_cycles. Total number of distinct cycles. Used purely for indexing
    - all_n_n. List with names of all nodes (whatever was used as keys in 'mechs_dtc')
    - all_e_n. List with names of all edges (whatever was used as keys in 'mechs_dtc')
    - typology_list. Iterable with typology indices for all mechanisms
    - tof_per_mech. Iterable with TOFs calculated for all mechanisms
    - hide_lessthan. Float, defines what 'very small' means when hiding Xtof values
    Output:
    - prints to STDOUT.
    '''
    fcw = 24 # First column width
    cw = 12 # Column width

    print("\n","-"*80,"\n  Degrees of TOF control (reduced)\n","-"*80)
    
    c_title = "\n".ljust(fcw+5)
    for c_idx in range(n_cycles): 
        c_title += f" cycle {c_idx+1}".ljust(cw)
    
    prnt = "     Intermediates" + c_title
    for n_n in all_n_n:
        _prnt = "\n" + n_n.ljust(fcw)

        show_row = False
        for c_idx in range(n_cycles):
            weights = []
            tofs_in_cycle = []
            for i, tof in enumerate(tof_per_mech):
                if typology_list[i] == c_idx:
                    tofs_in_cycle.append(tof)
            tofs_in_cycle = np.array(tofs_in_cycle)
            tof_sum = np.sum(tofs_in_cycle)
            weights = tofs_in_cycle / tof_sum
            
            Xtofs = []
            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    Xtofs.append(m_dict[n_n])
            Xtofs = np.array(Xtofs)
            weighted_Xtofs = weights * Xtofs
            Xtof_wavg = np.sum(weighted_Xtofs)
            
            _xtof = Xtof_wavg
            if _xtof < hide_lessthan: 
                _prnt += "-".rjust(cw)
            else: 
                _prnt += "%*.3g" % (cw, _xtof)
                show_row = True
        if show_row:
            prnt += _prnt
    print(prnt)

    prnt = "     Transition states"
    for e_n in all_e_n:
        _prnt = "\n" + e_n.replace("_"," - ").ljust(fcw)

        show_row = False
        for c_idx in range(n_cycles):
            weights = []
            tofs_in_cycle = []
            for i, tof in enumerate(tof_per_mech):
                if typology_list[i] == c_idx:
                    tofs_in_cycle.append(tof)
            tofs_in_cycle = np.array(tofs_in_cycle)
            tof_sum = np.sum(tofs_in_cycle)
            weights = tofs_in_cycle / tof_sum

            Xtofs = []
            for m_idx, m_dict in enumerate(mechs_dtc):
                # Check that the mechanism belongs to the cycle
                if typology_list[m_idx] == c_idx:
                    try:
                        Xtofs.append(m_dict[e_n])
                    except KeyError:
                        # Xtofs for transition states that are not included in the 
                        # mechanism are set to 0. Note that all mechanisms are still
                        # included in calculating the weights
                        Xtofs.append(0)
            Xtofs = np.array(Xtofs)
            weighted_Xtofs = weights * Xtofs
            Xtof_wavg = np.sum(weighted_Xtofs)
            
            _xtof = Xtof_wavg
            if _xtof < hide_lessthan: 
                _prnt += "-".rjust(cw)
            else: 
                _prnt += "%*.3g" % (cw, _xtof)
                show_row = True
        if show_row:
            prnt += _prnt
    print(prnt)

def degree_TOF_control(mech_container,temp,cycles,typology_list,tof_per_mech=None,
    reduced_output=False) -> None:
    '''
    Calculates degrees of turnover frequency control (Xtof) 
    Uses the equations as defined in Acc. Chem. Res. 2011, 44, 2, 101–110
    Branching is handled with the principles detailed in ACS Catal. 2015, 5, 9, 5242–5255
    Input:
    - mech_container. Pre-computed MechanismContainer.
    - temp. Float, temperature in K.
    - cycles. unique_cat_cycles. List of lists of edges defining a catalytic cycle typology.
    (Actually used only for indexing and needs to just be the right length.)
    - typology_list. Iterable with typology indices for all mechanisms
    - tof_per_mech. Optional. list of float TOF values in s-1 for each mechanism (as returned by 
    gtof.TOF_multimech). If supplied, the output for each state and each cycle in 'cycles' is
    returned as a weighted average of Xtofs from each mechanism belonging to the cycle. Xtofs of
    the mechanism with the highest TOF is also returned.
    - reduced_output. Boolean, only show weighted averages for each cycle. Anything very small
    is shown as '-' to reduce visual clutter. Completely hides rows with no significant Xtofs.
    - hide_lessthan. Float, defines what 'very small' means when reduced_output is True
    Output:
    - None, prints to STDOUT. 
    '''
    
    ### Constants:
    # Consider different units for R
    R_kcal=8.314462618/4184 #kcal mol-1 K-1
    R_kJ=8.314462618/1000 #kJ mol-1 K-1
    J_mol_to_eV=1/(1.602176634e-19*6.02214076e+23)
    R_eV=8.314462618*J_mol_to_eV
    R_values = {'kcal':R_kcal,'eV':R_eV,'kJ':R_kJ}
    Rvalue=R_values[mech_container.EUnits]
    RT=Rvalue*temp
    
    # 'mechs_dtc' becomes a list containing a dictionary for each mechanism with the Xtof values of
    # the intermediates and transition states calculated for that mechanism
    mechs_dtc = []

    all_n_n = []
    all_e_n = []

    for m_idx, m_orig in enumerate(mech_container.DirMechList):
        m = m_orig.copy() # Copy so that we don't delete nodes from the original mechanism

        main_cycle = m.graph['MainCatCycle']

        # Only include edges from the main cycle
        edges_tuples = main_cycle
        edges_tuples_r = [(e[1],e[0]) for e in main_cycle]
        edges_E = np.array([m.edges[e]['energy'] for e in edges_tuples]).reshape((1,-1))

        # The start node (I0) is not included in the calculations
        m.remove_node(m.graph['StartNode'])
        nodes = m.nodes(data=True)
        nodes_E = np.array([n[1]['energy'] for n in nodes]).reshape((-1,1))

        m_closing = m.graph['ClosingEdge']

        delta_array = []
        for n in nodes:
            # These lists can include edges that are in dead-end branches:
            e_before = n[1]['EdgesBefore']
            e_after = n[1]['EdgesAfter']

            # True if the node itself is part of a dead-end branch (closing edge not in 'e_after')
            n_in_branch = m_closing not in e_after


            # If node is in a branch, figure out which edges come in the main cycle after the 
            # branching-off point
            if n_in_branch:
                _e_after = []
                for past_e in e_before:
                    # Using 'm_orig' so that edges to and from I0 are present:
                    for after_past_e in m_orig.edges[past_e]['EdgesAfter']:
                        after_past_e_r = (after_past_e[1],after_past_e[0])

                        # Is 'after_past_e' after the branching point (not before current node):
                        e_after_branch=not (after_past_e in e_before or after_past_e_r in e_before)
                        # Is 'after_past_e' in the same branch but after n:
                        e_after_n = (after_past_e in e_after) or (after_past_e_r in e_after)
                        # Is 'after_past_e' in the main catalytic cycle for this mechanism:
                        e_in_cycle = (after_past_e in main_cycle) or (after_past_e_r in main_cycle)

                        if e_after_branch and (not e_after_n) and e_in_cycle:
                            _e_after.append(after_past_e)
                
                # Overwrite list of edges that come after n. Edges that are in the branch are lost
                # while still untraversed edges in the cycle are added
                e_after = _e_after
            
            # Get reaction energy from undirected mechanism with the same index
            Gr = mech_container.MechList[m_idx].graph['Greaction']
            
            # Determine dGi,'j between this node and all edges in the main cycle
            n_darray = []        
            for i in range(len(edges_tuples)):
                if edges_tuples[i] in e_after or edges_tuples_r[i] in e_after:
                    # I before TS -> dGi,'j = reaction energy
                    delta = Gr
                else:
                    # TS before I -> dGi,'j = 0
                    delta = 0
                n_darray.append(delta)
            delta_array.append(n_darray)
        
        # 'delta_array' becomes an array of shape (n,e) where n is the number of nodes and e is the
        # number of edges. It contains the dGi,'j term for each intermediate-transition state pair
        delta_array = np.array(delta_array)

        
        resist_exponents = ( edges_E - nodes_E - delta_array ) / RT # (Ti - Ij - dGi,'j) / RT
        dEs = edges_E - nodes_E - ( delta_array - Gr )

        # 'Catalytic resistance' for each intermediate-transition state pair in an array of
        # shape (n,e) where n is the number of nodes and e is the number of edges
        resist = np.exp(resist_exponents)

        resist_sum_all = np.sum(resist) # Sum of all e^((Ti - Ij - dGi,'j) / RT)
        resist_sum_pern = np.sum(resist,1) # Sum of e^((Ti - Ij - dGi,'j) / RT) for each I
        resist_sum_pere = np.sum(resist,0) # Sum of e^((Ti - Ij - dGi,'j) / RT) for each TS

        Xtof_n = resist_sum_pern / resist_sum_all # XTOF for each intermediate
        Xtof_e = resist_sum_pere / resist_sum_all # XTOF for each transition state

        # Save all calculated Xtofs for this mechanism into m_dtc and add it to mechs_dtc
        # Also keeps a list of all nodes and edges that are in the system
        m_dtc = {}
        for idx, n in enumerate(nodes):
            m_dtc[n[0]] = Xtof_n[idx]
            if not all_n_n.__contains__(n[0]):
                all_n_n.append(n[0])
        for idx, e in enumerate(edges_tuples):
            e_key = rg.tuple_to_string(e)
            m_dtc[e_key] = Xtof_e[idx]
            if not all_e_n.__contains__(e_key):
                all_e_n.append(e_key)
        mechs_dtc.append(m_dtc)
    

    # Print results
    if not reduced_output:
        if tof_per_mech == None:
            print_Xtof(mechs_dtc=mechs_dtc,n_cycles=len(cycles),all_n_n=all_n_n,all_e_n=all_e_n,
                typology_list=typology_list)        
        else:
            print_avg_Xtof(mechs_dtc=mechs_dtc,n_cycles=len(cycles),all_n_n=all_n_n,all_e_n=all_e_n,
                typology_list=typology_list,tof_per_mech=tof_per_mech)
    else:
        # Reduced output needs the TOF for each mechanism in order to work
        assert tof_per_mech != None
        print_reduced_Xtof(mechs_dtc=mechs_dtc,n_cycles=len(cycles),all_n_n=all_n_n,all_e_n=all_e_n,
            typology_list=typology_list,tof_per_mech=tof_per_mech)


    return None
        
        