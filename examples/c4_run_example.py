import nx_reaxgraf as rg
import nx_gTOFfee as gtof

G = rg.graph_instantiator("c4_nodes.txt","c4_edges.txt",
						  closing_edges=["4_1"])
mechanisms = rg.mechanism_fetcher(G)
TOF_list,dE_list,*_ = gtof.TOF_wrapper(mechanisms,temp=298.15,verbose=True)
for item in dE_list:
	print("dE = %6.2f kcal/mol" % item)


