
All indentations have been changed from tabs to spaces

rg.span_tree_generator and rg.mechanism_fetcher have been optimised by using NumPy arrays instead of matrices in order to make use of broadcasting

rg.plot_directed_network now allows one to give a 'pos' argument, which can be used to set the positions if parent_layout == False, eg.
        rg.plot_directed_network(G,parent_layout=False,pos=mech_container.PosMainGraph)

rg.mech_judge and rg.mech_typology_plot now try to fetch the positions from the main graph before creating new spring layouts.

rg.mech_judge now takes "figsize" as an argument

gtof.energy_summary_typologies now also prints the joined TOFs for each type
